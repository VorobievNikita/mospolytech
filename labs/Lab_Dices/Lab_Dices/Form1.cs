﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Dices
{
    public partial class fm : Form
    {
        DicesLib.Dices dices;
        public fm()
        {
            InitializeComponent();
            dices = new DicesLib.Dices();
        }

        private void BuStart_Click(object sender, EventArgs e)
        {
            dices.DoReset();
            int i = 3;
            int j = 3;
            foreach(Label c in tableLayoutPanel1.Controls)
            {
                c.Text = dices.matrix[j,i].ToString();
                i--;
                if(i<0)
                {
                    i = 3;
                    j--;
                }
            }
        }
    }
}
