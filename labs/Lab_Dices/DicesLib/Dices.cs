﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DicesLib
{
    public class Dices
    {
        public int[,] matrix { get; set; }
        public int RandomSteps { get; set; }
        public int CountSteps { get; set; }
        public void DoReset()
        {
            matrix = new int[4, 4]
            {
                {1,2,3,4 },
                {5,6,7,8 },
                {9,10,11,12 },
                {13,14,15,0 }
            };
            RandomSteps = 5;
            CountSteps = 0;
            int[] pos = new int[] { 3, 3 };
            Random rand = new Random();
            int Dir = 1;
            bool bord = true;
            for(int i = 0; i<RandomSteps;i++)
            {
                Dir = rand.Next(1, 4);
                int[] pos1 = pos;
                while(bord)
                {
                    switch (Dir)
                    {
                        case 1:
                            pos1[1] = pos1[1] - 1;
                            break;
                        case 2:
                            pos1[0] = pos1[0] - 1;
                            break;
                        case 3:
                            pos1[1] = pos1[1] + 1;
                            break;
                        case 4:
                            pos1[0] = pos1[0] + 1;
                            break;
                    }
                    if(pos1[0]>=0 && pos1[0]<4 && pos1[1] >= 0 && pos1[1] < 4)
                    {
                        bord = false;
                    }
                }
                matrix[pos[0],pos[1]] = matrix[pos1[0], pos1[1]];
                matrix[pos1[0], pos1[1]] = 0;
                pos = pos1;
            }
        }
    }
}
