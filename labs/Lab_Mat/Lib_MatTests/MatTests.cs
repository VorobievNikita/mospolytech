﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lib_Mat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Mat.Tests
{
    [TestClass()]
    public class MatTests
    {
        [TestMethod()]
        public void AddTest5()
        {
            int a = 2;
            int b = 3;
            int res;
            Mat matLib = new Mat();
            res = matLib.Add(a, b);
            Assert.AreEqual(res, 5);
        }

        [TestMethod()]
        public void AddTest3()
        {
            int a = 1;
            int b = 2;
            int res;
            Mat matLib = new Mat();
            res = matLib.Add(a, b);
            Assert.AreEqual(res, 3);
        }

        [TestMethod()]
        public void MultTest14()
        {
            int a = 2;
            int b = 7;
            int res;
            Mat matLib = new Mat();
            res = matLib.Mult(a, b);
            Assert.AreEqual(res, 14);
        }

        [TestMethod()]
        public void SquareTest9()
        {
            int a = 3;
            int res;
            Mat matLib = new Mat();
            res = matLib.Square(a);
            Assert.AreEqual(res, 9);
        }
    }
}