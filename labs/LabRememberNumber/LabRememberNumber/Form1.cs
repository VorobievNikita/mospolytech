﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;

namespace LabRememberNumber
{
    public partial class fm : Form
    {
        private Core.Game g;
        public fm()
        {
            InitializeComponent();
            g = new Game();
            g.GoScreenRemember += Event_GoScreenRemember;
            g.GoScreenAnswer += Event_GoScreenAnswer;
            g.DoReset();
            buPlay.Click += buAnswer_Click;
        }

        private void buAnswer_Click(object sender, EventArgs e)
        {
            int x;
            int.TryParse(tbNum.Text, out x);
            g.DoAnswer(x);
        }

        private void Event_GoScreenAnswer(object sender, EventArgs e)
        {
            tbNum.Text = "";
            laRemember.Visible = false;
            tlp.Visible = false;
            tbNum.Visible = true;
            laAnswer.Visible = true;
            laNum.Visible = false;
            buPlay.Visible = true;
        }

        private void Event_GoScreenRemember(object sender, EventArgs e)
        {
            laCor.Text = String.Format("Верно = {0}", g.CountCorrect.ToString());
            laIrc.Text = String.Format("Неверно = {0}", g.CountWrong.ToString());
            laNum.Text = g.Code.ToString();
            laRemember.Visible = true;
            tlp.Visible = true;
            laAnswer.Visible = false;
            tbNum.Visible = false;
            buPlay.Visible = false;
            laNum.Visible = true;
        }
    }
}
