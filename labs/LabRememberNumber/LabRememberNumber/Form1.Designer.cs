﻿namespace LabRememberNumber
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlp = new System.Windows.Forms.TableLayoutPanel();
            this.laCor = new System.Windows.Forms.Label();
            this.laIrc = new System.Windows.Forms.Label();
            this.laRemember = new System.Windows.Forms.Label();
            this.laNum = new System.Windows.Forms.Label();
            this.laAnswer = new System.Windows.Forms.Label();
            this.tbNum = new System.Windows.Forms.TextBox();
            this.buPlay = new System.Windows.Forms.Button();
            this.tlp.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp
            // 
            this.tlp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlp.ColumnCount = 2;
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp.Controls.Add(this.laIrc, 1, 0);
            this.tlp.Controls.Add(this.laCor, 0, 0);
            this.tlp.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tlp.Location = new System.Drawing.Point(12, 12);
            this.tlp.Name = "tlp";
            this.tlp.RowCount = 1;
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp.Size = new System.Drawing.Size(460, 60);
            this.tlp.TabIndex = 0;
            // 
            // laCor
            // 
            this.laCor.AutoSize = true;
            this.laCor.BackColor = System.Drawing.Color.LightGreen;
            this.laCor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laCor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laCor.Location = new System.Drawing.Point(3, 0);
            this.laCor.Name = "laCor";
            this.laCor.Size = new System.Drawing.Size(224, 60);
            this.laCor.TabIndex = 1;
            this.laCor.Text = "Верно: 0";
            this.laCor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laIrc
            // 
            this.laIrc.AutoSize = true;
            this.laIrc.BackColor = System.Drawing.Color.LightCoral;
            this.laIrc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laIrc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laIrc.Location = new System.Drawing.Point(233, 0);
            this.laIrc.Name = "laIrc";
            this.laIrc.Size = new System.Drawing.Size(224, 60);
            this.laIrc.TabIndex = 2;
            this.laIrc.Text = "Неверно: 0";
            this.laIrc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laRemember
            // 
            this.laRemember.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laRemember.AutoSize = true;
            this.laRemember.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laRemember.Location = new System.Drawing.Point(171, 75);
            this.laRemember.Name = "laRemember";
            this.laRemember.Size = new System.Drawing.Size(143, 20);
            this.laRemember.TabIndex = 1;
            this.laRemember.Text = "Запомните число";
            // 
            // laNum
            // 
            this.laNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laNum.Location = new System.Drawing.Point(12, 97);
            this.laNum.Name = "laNum";
            this.laNum.Size = new System.Drawing.Size(457, 132);
            this.laNum.TabIndex = 2;
            this.laNum.Text = "000000";
            this.laNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laAnswer
            // 
            this.laAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laAnswer.Location = new System.Drawing.Point(15, 229);
            this.laAnswer.Name = "laAnswer";
            this.laAnswer.Size = new System.Drawing.Size(457, 23);
            this.laAnswer.TabIndex = 3;
            this.laAnswer.Text = "Введите число";
            this.laAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbNum
            // 
            this.tbNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNum.Location = new System.Drawing.Point(12, 255);
            this.tbNum.Name = "tbNum";
            this.tbNum.Size = new System.Drawing.Size(460, 20);
            this.tbNum.TabIndex = 4;
            this.tbNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buPlay
            // 
            this.buPlay.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buPlay.Location = new System.Drawing.Point(0, 403);
            this.buPlay.Name = "buPlay";
            this.buPlay.Size = new System.Drawing.Size(484, 58);
            this.buPlay.TabIndex = 5;
            this.buPlay.Text = "Подтвердить";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.buPlay);
            this.Controls.Add(this.tbNum);
            this.Controls.Add(this.laAnswer);
            this.Controls.Add(this.laNum);
            this.Controls.Add(this.laRemember);
            this.Controls.Add(this.tlp);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "fm";
            this.Text = "LabRememberNumber";
            this.tlp.ResumeLayout(false);
            this.tlp.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlp;
        private System.Windows.Forms.Label laIrc;
        private System.Windows.Forms.Label laCor;
        private System.Windows.Forms.Label laRemember;
        private System.Windows.Forms.Label laNum;
        private System.Windows.Forms.Label laAnswer;
        private System.Windows.Forms.TextBox tbNum;
        private System.Windows.Forms.Button buPlay;
    }
}

