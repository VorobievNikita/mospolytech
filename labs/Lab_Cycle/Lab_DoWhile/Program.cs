﻿using System;

namespace Lab_DoWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            do
            {
                Console.WriteLine("i = {0}", i);
                i++;
            } while (i < 0);
            Console.WriteLine("end");
            Console.ReadKey();
        }
    }
}
