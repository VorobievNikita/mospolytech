﻿using System;

namespace Lab_Foreach
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = { 1, 2, 3, 4 };
            foreach(int i in x)
            {
                Console.WriteLine("i = {0}", i);
            }
            Console.WriteLine("end");
            Console.ReadKey();
        }
    }
}
