﻿using System;

namespace Lab_For
{
    class Program
    {
        static void Main(string[] args)
        {
            AAA(6);
            AAA1(7);
        }
        private static void AAA(int x)
        {
            for (int i = 0; i < x; i+=2)
            {
                Console.WriteLine("i = {0}", i);
            }
            Console.WriteLine("end");
            Console.ReadKey();
        }
        private static void AAA1(int x)
        {
            for (int i = 1; i < x; i *= 2)
            {
                Console.WriteLine("i = {0}", i);
            }
            Console.WriteLine("end");
            Console.ReadKey();
        }
    }
}
