﻿using System;

namespace Lab_recusion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Fact(3));
        }
        private static int Fact(int x)
        {
            Console.WriteLine("x = {0}", x);
            if(x == 0)
            {
                return 1;
            }
            else
            {
                return x * Fact(x - 1);
            }
        }
    }
}
