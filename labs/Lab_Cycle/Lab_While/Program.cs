﻿using System;

namespace Lab_While
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            while(i<4)
            {
                Console.WriteLine("i = {0}", i);
                i++;
            }
            Console.WriteLine("end");
            Console.ReadKey();
        }
    }
}
