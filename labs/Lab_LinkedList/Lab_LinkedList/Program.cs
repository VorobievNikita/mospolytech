﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<string> x = new LinkedList<string>();

            x.AddFirst("Мир");
            x.AddLast("Труд");
            x.AddAfter(x.Last, "Май");
            foreach(string item in x)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }
}
