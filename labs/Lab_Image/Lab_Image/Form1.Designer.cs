﻿namespace Lab_Image
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.picB1 = new System.Windows.Forms.PictureBox();
            this.imgL1 = new System.Windows.Forms.ImageList(this.components);
            this.bu1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.picB1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // picB1
            // 
            this.picB1.Location = new System.Drawing.Point(12, 12);
            this.picB1.Name = "picB1";
            this.picB1.Size = new System.Drawing.Size(164, 192);
            this.picB1.TabIndex = 0;
            this.picB1.TabStop = false;
            // 
            // imgL1
            // 
            this.imgL1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgL1.ImageStream")));
            this.imgL1.TransparentColor = System.Drawing.Color.Transparent;
            this.imgL1.Images.SetKeyName(0, "1fec2cb781c9ff11ece4207162d6b259.png");
            this.imgL1.Images.SetKeyName(1, "41RU74b+K8L.jpg");
            // 
            // bu1
            // 
            this.bu1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bu1.ImageIndex = 1;
            this.bu1.ImageList = this.imgL1;
            this.bu1.Location = new System.Drawing.Point(12, 210);
            this.bu1.Name = "bu1";
            this.bu1.Size = new System.Drawing.Size(164, 239);
            this.bu1.TabIndex = 1;
            this.bu1.Text = "Кнопка";
            this.bu1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.ImageList = this.imgL1;
            this.tabControl1.Location = new System.Drawing.Point(182, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(490, 437);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.ImageIndex = 0;
            this.tabPage1.Location = new System.Drawing.Point(4, 135);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(482, 170);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Геральт";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.ImageIndex = 1;
            this.tabPage2.Location = new System.Drawing.Point(4, 135);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(482, 298);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ворон";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 461);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.bu1);
            this.Controls.Add(this.picB1);
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "fm";
            this.Text = "Lab_Image";
            ((System.ComponentModel.ISupportInitialize)(this.picB1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picB1;
        private System.Windows.Forms.ImageList imgL1;
        private System.Windows.Forms.Button bu1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}

