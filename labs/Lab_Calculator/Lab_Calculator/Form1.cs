﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private int Plus(int a, int b)
        {
            int c = a + b;
            return c;
        }
        private int Minus(int a, int b)
        {
            int c = a - b;
            return c;
        }
        private int Mult(int a, int b)
        {
            int c = a * b;
            return c;
        }
        private int Div(int a, int b)
        {
            int c = a / b;
            return c;
        }
        private void BuPlus_Click(object sender, EventArgs e)
        {
            try
            {
                laAns.Text = Plus(int.Parse(tb1.Text), int.Parse(tb2.Text)).ToString();
            }
            catch(Exception exe)
            {
                laAns.Text = "Входные данные неверны!";
            }
        }

        private void BuAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Melkov Vadim 194-321");
        }

        private void BuMinus_Click(object sender, EventArgs e)
        {
            try
            {
                laAns.Text = Minus(int.Parse(tb1.Text), int.Parse(tb2.Text)).ToString();
            }
            catch (Exception exe)
            {
                laAns.Text = "Входные данные неверны!";
            }
        }

        private void BuMult_Click(object sender, EventArgs e)
        {
            try
            {
                laAns.Text = Mult(int.Parse(tb1.Text), int.Parse(tb2.Text)).ToString();
            }
            catch (Exception exe)
            {
                laAns.Text = "Входные данные неверны!";
            }
        }

        private void BuDiv_Click(object sender, EventArgs e)
        {
            try
            {
                laAns.Text = Div(int.Parse(tb1.Text), int.Parse(tb2.Text)).ToString();
            }
            catch (Exception exe)
            {
                laAns.Text = "Входные данные неверны!";
            }
        }
    }
}
