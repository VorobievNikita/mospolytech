﻿namespace Lab_Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tb1 = new System.Windows.Forms.TextBox();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buPlus = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.laAns = new System.Windows.Forms.Label();
            this.buAbout = new System.Windows.Forms.Button();
            this.buMinus = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buDiv = new System.Windows.Forms.Button();
            this.buMult = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tb1
            // 
            this.tb1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb1.Location = new System.Drawing.Point(61, 4);
            this.tb1.Name = "tb1";
            this.tb1.Size = new System.Drawing.Size(100, 30);
            this.tb1.TabIndex = 0;
            this.tb1.Text = "0";
            this.tb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb2
            // 
            this.tb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb2.Location = new System.Drawing.Point(61, 40);
            this.tb2.Name = "tb2";
            this.tb2.Size = new System.Drawing.Size(100, 30);
            this.tb2.TabIndex = 1;
            this.tb2.Text = "0";
            this.tb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "A =";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "B =";
            // 
            // buPlus
            // 
            this.buPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buPlus.Location = new System.Drawing.Point(17, 76);
            this.buPlus.Name = "buPlus";
            this.buPlus.Size = new System.Drawing.Size(75, 52);
            this.buPlus.TabIndex = 4;
            this.buPlus.Text = "+";
            this.buPlus.UseVisualStyleBackColor = true;
            this.buPlus.Click += new System.EventHandler(this.BuPlus_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 427);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Ответ:";
            // 
            // laAns
            // 
            this.laAns.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.laAns.AutoSize = true;
            this.laAns.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laAns.Location = new System.Drawing.Point(96, 427);
            this.laAns.Name = "laAns";
            this.laAns.Size = new System.Drawing.Size(23, 25);
            this.laAns.TabIndex = 6;
            this.laAns.Text = "0";
            // 
            // buAbout
            // 
            this.buAbout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buAbout.Location = new System.Drawing.Point(364, 397);
            this.buAbout.Name = "buAbout";
            this.buAbout.Size = new System.Drawing.Size(108, 52);
            this.buAbout.TabIndex = 7;
            this.buAbout.Text = "About";
            this.buAbout.UseVisualStyleBackColor = true;
            this.buAbout.Click += new System.EventHandler(this.BuAbout_Click);
            // 
            // buMinus
            // 
            this.buMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buMinus.Location = new System.Drawing.Point(101, 76);
            this.buMinus.Name = "buMinus";
            this.buMinus.Size = new System.Drawing.Size(75, 52);
            this.buMinus.TabIndex = 8;
            this.buMinus.Text = "-";
            this.buMinus.UseVisualStyleBackColor = true;
            this.buMinus.Click += new System.EventHandler(this.BuMinus_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(331, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(141, 116);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // buDiv
            // 
            this.buDiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buDiv.Location = new System.Drawing.Point(101, 143);
            this.buDiv.Name = "buDiv";
            this.buDiv.Size = new System.Drawing.Size(75, 52);
            this.buDiv.TabIndex = 10;
            this.buDiv.Text = "/";
            this.buDiv.UseVisualStyleBackColor = true;
            this.buDiv.Click += new System.EventHandler(this.BuDiv_Click);
            // 
            // buMult
            // 
            this.buMult.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buMult.Location = new System.Drawing.Point(17, 143);
            this.buMult.Name = "buMult";
            this.buMult.Size = new System.Drawing.Size(75, 52);
            this.buMult.TabIndex = 11;
            this.buMult.Text = "*";
            this.buMult.UseVisualStyleBackColor = true;
            this.buMult.Click += new System.EventHandler(this.BuMult_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.buMult);
            this.Controls.Add(this.buDiv);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buMinus);
            this.Controls.Add(this.buAbout);
            this.Controls.Add(this.laAns);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buPlus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb2);
            this.Controls.Add(this.tb1);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "Form1";
            this.Text = "Lab_Calculator";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.TextBox tb2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buPlus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label laAns;
        private System.Windows.Forms.Button buAbout;
        private System.Windows.Forms.Button buMinus;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buDiv;
        private System.Windows.Forms.Button buMult;
    }
}

