﻿using System;

namespace Lab_Student
{
    delegate void StudentHandler(Student sender, EventChangeStudent e);

    public class EventChangeStudent
    {
        public int Age
        { get; }
        public string Message
        { get; }
        public string Name
        { get; }
        public EventChangeStudent(string message, int age)
        {
            Age = age;
            Message = message;
        }
        public EventChangeStudent(string message, string name)
        {
            Name = name;
            Message = message;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            student.ChangeAge += Display;
            student.ChangeName += Display1;

            student.Name = "Vadim";
            student.Surname = "Melkov";
            student.age = 22;
            Console.WriteLine(student.GetFullName());
            student.age = 17;
            Console.WriteLine(student.GetFullName());
            Console.ReadKey();
        }
        static void Display(Student sender, EventChangeStudent e)
        {
            Console.WriteLine("{0} {1} {2}", e.Message, sender.Name, e.Age);
        }
        static void Display1(Student sender, EventChangeStudent e)
        {
            Console.WriteLine("{0} {1}", e.Message, e.Name);
        }
    }
}
