﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_Student
{
    class Student
    {
        public event StudentHandler ChangeAge;
        public event StudentHandler ChangeName;
        private int _age;
        private string _name;
        public int age
        {
            get
            {
                return _age;
            }
            set
            {
                if(value>0)
                {
                    _age = value;
                    ChangeAge?.Invoke(this, new EventChangeStudent("Возраст студента ", _age));
                }
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length > 0)
                {
                    _name = value;
                    ChangeName?.Invoke(this, new EventChangeStudent("Имя студента изменено на", _name));
                }
            }
        }
        public string Surname
        {
            get;
            set;
        }
        public string GetFullName()
        {
            return Name + " " + Surname + " (" + _age + ")";
        }
    }
}
