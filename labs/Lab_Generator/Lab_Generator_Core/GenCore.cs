﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Generator_Core
{
    public class GenCore
    {
        public static string RandomStr(int aLength, bool aDown, bool aUp, bool aNum, bool aSpec, bool aRu)
        {
            string c1 = "abcdefghijklmnopqrstuvwxyz";
            string c2 = "1234567890";
            string c3 = "[]{}<>,.;:-+*/=@$&";
            string c4 = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

            var x = new StringBuilder();
            var xResult = new StringBuilder();
            Random rnd = new Random();

            if (aNum) x.Append(c2);
            if (aSpec) x.Append(c3);
            if (aRu)
            {
                if(aDown)
                {
                    x.Append(c4);
                }
                if(aUp)
                {
                    x.Append(c4.ToUpper());
                }
                if (x.ToString() == "") x.Append(c4);
            }
            else
            {
                if (aDown)
                {
                    x.Append(c1);
                }
                if (aUp)
                {
                    x.Append(c1.ToUpper());
                }
                if (x.ToString() == "") x.Append(c1);
            }

            while(xResult.Length<aLength)
            {
                xResult.Append(x[rnd.Next(x.Length)]);
            }
            return xResult.ToString();
        }
    }
}
