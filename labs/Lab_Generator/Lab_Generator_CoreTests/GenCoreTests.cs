﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab_Generator_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Generator_Core.Tests
{
    [TestClass()]
    public class GenCoreTests
    {
        [TestMethod()]
        public void RandomStrTestLength10()
        {
            int a = 10;
            string res = GenCore.RandomStr(a, true, true, true, true, true);
            Assert.AreEqual(res.Length, a);
        }
        [TestMethod()]
        public void RandomStrTestDownTrue()
        {
            bool a = true;
            string res = GenCore.RandomStr(10, a, false, false, false, false);
            int b = res.Count(char.IsLower);
            Assert.AreEqual(b>0, a);
        }
        [TestMethod()]
        public void RandomStrTestUpTrue()
        {
            bool a = true;
            string res = GenCore.RandomStr(10, false, a, false, false, false);
            int b = res.Count(char.IsUpper);
            Assert.AreEqual(b > 0, a);
        }
        [TestMethod()]
        public void RandomStrTestNumTrue()
        {
            bool a = true;
            bool b = false;
            string num = "1234567890";
            string res = GenCore.RandomStr(10, false, false, a, false, false);
            for(int i = 0; i<res.Length;i++)
            {
                for(int j = 0; j< num.Length;j++)
                {
                    if(res[i] == num[j])
                    {
                        b = true;
                    }
                }
            }
            Assert.AreEqual(b, a);
        }
        [TestMethod()]
        public void RandomStrTestSpecTrue()
        {
            bool a = true;
            bool b = false;
            string spec = "[]{}<>,.;:-+*/=@$&";
            string res = GenCore.RandomStr(10, false, false, false, a, false);
            for (int i = 0; i < res.Length; i++)
            {
                for (int j = 0; j < spec.Length; j++)
                {
                    if (res[i] == spec[j])
                    {
                        b = true;
                    }
                }
            }
            Assert.AreEqual(b, a);
        }
    }
}