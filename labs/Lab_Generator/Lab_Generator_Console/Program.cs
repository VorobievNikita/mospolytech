﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab_Generator_Core;

namespace Lab_Generator_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            string ans;
            bool bAns = true;
            Console.WriteLine("Генератор пароля\n");
            while (bAns)
            {
                Console.WriteLine("Пароль должен содержать спецсимволы?\n'+' - да\n'-' - нет\n");
                ans = Console.ReadLine();
                bool specSymb = getBAns(ans);

                Console.WriteLine("Пароль должен содержать символы нижнего регистра?\n'+' - да\n'-' - нет\n");
                ans = Console.ReadLine();
                bool aLower = getBAns(ans);

                Console.WriteLine("Пароль должен содержать символы верхнего регистра?\n'+' - да\n'-' - нет\n");
                ans = Console.ReadLine();
                bool aUpper = getBAns(ans);

                Console.WriteLine("Пароль должен содержать числа?\n'+' - да\n'-' - нет\n");
                ans = Console.ReadLine();
                bool numb = getBAns(ans);

                Console.WriteLine("Пароль должен содержать русские символы?\n'+' - да\n'-' - нет\n");
                ans = Console.ReadLine();
                bool ruSymb = getBAns(ans);

                int length;
                Console.WriteLine("Введите длину пароля\n");
                length = int.Parse(Console.ReadLine());

                Console.WriteLine(GenCore.RandomStr(length, aLower, aUpper, numb, specSymb, ruSymb));
                Console.WriteLine("Сгенерировать ещё один пароль?\n'+' - да\n'-' - нет\n");
                ans = Console.ReadLine();
                bAns = getBAns(ans);
            }
            Console.WriteLine("Нажмите любую клавишу для выхода...\n");
            Console.ReadLine();
        }

        public static bool getBAns(string ans)
        {
            bool tmp;
            if (ans == "+")
            {
                tmp = true;
            }
            else
            {
                if(ans != "-")Console.WriteLine("Вы ввели неправильный ответ. Параметр по умолчанию отключен\n");
                tmp = false;
            }
            return tmp;
        }
    }
}