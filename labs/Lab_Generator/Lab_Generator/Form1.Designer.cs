﻿namespace Lab_Generator
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.tbMain = new System.Windows.Forms.TextBox();
            this.buGen = new System.Windows.Forms.Button();
            this.chbDown = new System.Windows.Forms.CheckBox();
            this.chbUp = new System.Windows.Forms.CheckBox();
            this.chbNum = new System.Windows.Forms.CheckBox();
            this.chbSpec = new System.Windows.Forms.CheckBox();
            this.la1 = new System.Windows.Forms.Label();
            this.numLength = new System.Windows.Forms.NumericUpDown();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chbRu = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbMain
            // 
            this.tbMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbMain.Location = new System.Drawing.Point(12, 12);
            this.tbMain.Name = "tbMain";
            this.tbMain.Size = new System.Drawing.Size(460, 32);
            this.tbMain.TabIndex = 0;
            this.tbMain.Text = "<Password>";
            this.tbMain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buGen
            // 
            this.buGen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buGen.Location = new System.Drawing.Point(12, 50);
            this.buGen.Name = "buGen";
            this.buGen.Size = new System.Drawing.Size(460, 55);
            this.buGen.TabIndex = 1;
            this.buGen.Text = "Генерировать";
            this.buGen.UseVisualStyleBackColor = true;
            // 
            // chbDown
            // 
            this.chbDown.AutoSize = true;
            this.chbDown.Location = new System.Drawing.Point(13, 112);
            this.chbDown.Name = "chbDown";
            this.chbDown.Size = new System.Drawing.Size(174, 17);
            this.chbDown.TabIndex = 2;
            this.chbDown.Text = "Символы в нижнем регистре";
            this.chbDown.UseVisualStyleBackColor = true;
            // 
            // chbUp
            // 
            this.chbUp.AutoSize = true;
            this.chbUp.Location = new System.Drawing.Point(13, 135);
            this.chbUp.Name = "chbUp";
            this.chbUp.Size = new System.Drawing.Size(177, 17);
            this.chbUp.TabIndex = 3;
            this.chbUp.Text = "Символы в верхнем регистре";
            this.chbUp.UseVisualStyleBackColor = true;
            // 
            // chbNum
            // 
            this.chbNum.AutoSize = true;
            this.chbNum.Location = new System.Drawing.Point(13, 158);
            this.chbNum.Name = "chbNum";
            this.chbNum.Size = new System.Drawing.Size(62, 17);
            this.chbNum.TabIndex = 4;
            this.chbNum.Text = "Цифры";
            this.chbNum.UseVisualStyleBackColor = true;
            // 
            // chbSpec
            // 
            this.chbSpec.AutoSize = true;
            this.chbSpec.Location = new System.Drawing.Point(13, 181);
            this.chbSpec.Name = "chbSpec";
            this.chbSpec.Size = new System.Drawing.Size(97, 17);
            this.chbSpec.TabIndex = 5;
            this.chbSpec.Text = "Срецсимволы";
            this.chbSpec.UseVisualStyleBackColor = true;
            // 
            // la1
            // 
            this.la1.AutoSize = true;
            this.la1.Location = new System.Drawing.Point(10, 224);
            this.la1.Name = "la1";
            this.la1.Size = new System.Drawing.Size(82, 13);
            this.la1.TabIndex = 6;
            this.la1.Text = "Длина пароля:";
            // 
            // numLength
            // 
            this.numLength.Location = new System.Drawing.Point(98, 224);
            this.numLength.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numLength.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numLength.Name = "numLength";
            this.numLength.Size = new System.Drawing.Size(120, 20);
            this.numLength.TabIndex = 7;
            this.numLength.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(10, 250);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(462, 199);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // chbRu
            // 
            this.chbRu.AutoSize = true;
            this.chbRu.Location = new System.Drawing.Point(12, 204);
            this.chbRu.Name = "chbRu";
            this.chbRu.Size = new System.Drawing.Size(102, 17);
            this.chbRu.TabIndex = 9;
            this.chbRu.Text = "Русские буквы";
            this.chbRu.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.chbRu);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.numLength);
            this.Controls.Add(this.la1);
            this.Controls.Add(this.chbSpec);
            this.Controls.Add(this.chbNum);
            this.Controls.Add(this.chbUp);
            this.Controls.Add(this.chbDown);
            this.Controls.Add(this.buGen);
            this.Controls.Add(this.tbMain);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "fm";
            this.Text = "Lab_Generator";
            ((System.ComponentModel.ISupportInitialize)(this.numLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbMain;
        private System.Windows.Forms.Button buGen;
        private System.Windows.Forms.CheckBox chbDown;
        private System.Windows.Forms.CheckBox chbUp;
        private System.Windows.Forms.CheckBox chbNum;
        private System.Windows.Forms.CheckBox chbSpec;
        private System.Windows.Forms.Label la1;
        private System.Windows.Forms.NumericUpDown numLength;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox chbRu;
    }
}

