﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab_Generator_Core;

namespace Lab_Generator
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            buGen.Click += BuGen_Click;
        }

        private void BuGen_Click(object sender, EventArgs e)
        {
            tbMain.Text = GenCore.RandomStr((int)numLength.Value, chbDown.Checked, chbUp.Checked,
                chbNum.Checked,chbSpec.Checked, chbRu.Checked);
        }
    }
}
