﻿namespace LabPyatnashki {
    partial class MainForm {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent() {
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.toolStripNewGame = new System.Windows.Forms.ToolStripButton();
            this.toolStripComboBoxMatrixSize = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabelMatrixSize = new System.Windows.Forms.ToolStripLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.labelRandomizeField = new System.Windows.Forms.Label();
            this.numericUDRandomizeField = new System.Windows.Forms.NumericUpDown();
            this.labelCountOfUserSwappes = new System.Windows.Forms.Label();
            this.tableLPGameField = new System.Windows.Forms.TableLayoutPanel();
            this.toolStripMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUDRandomizeField)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripNewGame,
            this.toolStripComboBoxMatrixSize,
            this.toolStripLabelMatrixSize});
            this.toolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.Size = new System.Drawing.Size(385, 25);
            this.toolStripMenu.TabIndex = 0;
            this.toolStripMenu.Text = "toolStripMenu";
            // 
            // toolStripNewGame
            // 
            this.toolStripNewGame.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripNewGame.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripNewGame.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripNewGame.Name = "toolStripNewGame";
            this.toolStripNewGame.Size = new System.Drawing.Size(76, 22);
            this.toolStripNewGame.Text = "Новая игра";
            this.toolStripNewGame.ToolTipText = "Новая игра";
            this.toolStripNewGame.Click += new System.EventHandler(this.toolStripNewGame_Click);
            // 
            // toolStripComboBoxMatrixSize
            // 
            this.toolStripComboBoxMatrixSize.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripComboBoxMatrixSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxMatrixSize.Items.AddRange(new object[] {
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.toolStripComboBoxMatrixSize.Name = "toolStripComboBoxMatrixSize";
            this.toolStripComboBoxMatrixSize.Size = new System.Drawing.Size(75, 25);
            this.toolStripComboBoxMatrixSize.Sorted = true;
            // 
            // toolStripLabelMatrixSize
            // 
            this.toolStripLabelMatrixSize.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabelMatrixSize.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripLabelMatrixSize.Name = "toolStripLabelMatrixSize";
            this.toolStripLabelMatrixSize.Size = new System.Drawing.Size(82, 22);
            this.toolStripLabelMatrixSize.Text = "Размер игры";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // labelRandomizeField
            // 
            this.labelRandomizeField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRandomizeField.AutoSize = true;
            this.labelRandomizeField.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRandomizeField.Location = new System.Drawing.Point(12, 29);
            this.labelRandomizeField.Name = "labelRandomizeField";
            this.labelRandomizeField.Size = new System.Drawing.Size(139, 17);
            this.labelRandomizeField.TabIndex = 1;
            this.labelRandomizeField.Text = "Шаги запутанности:";
            // 
            // numericUDRandomizeField
            // 
            this.numericUDRandomizeField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUDRandomizeField.Location = new System.Drawing.Point(157, 28);
            this.numericUDRandomizeField.Name = "numericUDRandomizeField";
            this.numericUDRandomizeField.Size = new System.Drawing.Size(37, 20);
            this.numericUDRandomizeField.TabIndex = 2;
            this.numericUDRandomizeField.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUDRandomizeField.ValueChanged += new System.EventHandler(this.numericUDRandomizeField_ValueChanged);
            // 
            // labelCountOfUserSwappes
            // 
            this.labelCountOfUserSwappes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCountOfUserSwappes.AutoSize = true;
            this.labelCountOfUserSwappes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCountOfUserSwappes.Location = new System.Drawing.Point(208, 29);
            this.labelCountOfUserSwappes.Name = "labelCountOfUserSwappes";
            this.labelCountOfUserSwappes.Size = new System.Drawing.Size(165, 17);
            this.labelCountOfUserSwappes.TabIndex = 3;
            this.labelCountOfUserSwappes.Text = "Кол-во перемещений: 0";
            // 
            // tableLPGameField
            // 
            this.tableLPGameField.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLPGameField.ColumnCount = 1;
            this.tableLPGameField.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLPGameField.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLPGameField.Location = new System.Drawing.Point(0, 49);
            this.tableLPGameField.Name = "tableLPGameField";
            this.tableLPGameField.RowCount = 1;
            this.tableLPGameField.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLPGameField.Size = new System.Drawing.Size(385, 296);
            this.tableLPGameField.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGreen;
            this.ClientSize = new System.Drawing.Size(385, 345);
            this.Controls.Add(this.tableLPGameField);
            this.Controls.Add(this.labelCountOfUserSwappes);
            this.Controls.Add(this.numericUDRandomizeField);
            this.Controls.Add(this.labelRandomizeField);
            this.Controls.Add(this.toolStripMenu);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "LabPyatnashki";
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUDRandomizeField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton toolStripNewGame;
        private System.Windows.Forms.ToolStripLabel toolStripLabelMatrixSize;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxMatrixSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelRandomizeField;
        private System.Windows.Forms.NumericUpDown numericUDRandomizeField;
        private System.Windows.Forms.Label labelCountOfUserSwappes;
        private System.Windows.Forms.TableLayoutPanel tableLPGameField;
    }
}

