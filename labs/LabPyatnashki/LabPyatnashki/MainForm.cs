﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabPyatnashki {
    public partial class MainForm : Form {
        Games games;
        
        public int fieldSize { get; set; }
        public MainForm() {
            InitializeComponent();

            games = new Games(
                (int) this.numericUDRandomizeField.Value
                );

            this.toolStripComboBoxMatrixSize.SelectedIndexChanged += ToolStripComboBoxMatrixSize_SelectedIndexChanged;
            this.toolStripComboBoxMatrixSize.SelectedIndex = 1;
        }

        private void ToolStripComboBoxMatrixSize_SelectedIndexChanged(object sender, EventArgs e) {
            fieldSize = Convert.ToInt32(this.toolStripComboBoxMatrixSize.SelectedItem);
            games.DoReset(fieldSize);
            clearTableLayoutPanel();
            setTableLayoutPanel(fieldSize);
        }

        private void clearTableLayoutPanel() {
            this.tableLPGameField.Visible = false;
            this.tableLPGameField.ColumnStyles.Clear();
            this.tableLPGameField.RowStyles.Clear();
            this.tableLPGameField.Controls.Clear();
        }

        private void setTableLayoutPanel(int fieldSize) {

            this.tableLPGameField.ColumnCount = fieldSize;
            this.tableLPGameField.RowCount = fieldSize;

            for (int i = 0; i < fieldSize; ++i) {
                this.tableLPGameField.RowStyles.Add(new RowStyle(SizeType.Percent, (float) (100 / fieldSize)));
                this.tableLPGameField.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, (float) (100 / fieldSize)));
            }

            for (int i = 0; i < fieldSize; ++i) {
                for (int j = 0; j < fieldSize; ++j) {
                    addNewLabel(i, j);
                }
            }

            this.tableLPGameField.Visible = true;
        }

        private void addNewLabel (int row, int column) {
            if (games.gameField[row, column] == 0) {
                return;
            }

            Label label = new Label();
            label.AutoSize = true;
            label.Text = games.gameField[row, column].ToString();
            label.Margin = new Padding(0);
            label.Dock = DockStyle.Fill;
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Click += Label_Click;
            this.tableLPGameField.Controls.Add(label);
            this.tableLPGameField.SetCellPosition(label, new TableLayoutPanelCellPosition(column, row));
        }

        private void Label_Click(object sender, EventArgs e) {
            int[] zeroPos = games.findZero(games.gameField);

            Label label = (Label) sender;

            TableLayoutPanelCellPosition labelCellPosition = this.tableLPGameField.GetPositionFromControl(label);

            if (
                ((Math.Abs(zeroPos[0] - labelCellPosition.Row) == 1) && (Math.Abs(zeroPos[1] - labelCellPosition.Column) == 0)) ||
                ((Math.Abs(zeroPos[0] - labelCellPosition.Row) == 0) && (Math.Abs(zeroPos[1] - labelCellPosition.Column) == 1))
                ){

                this.tableLPGameField.SetCellPosition(
                    label,
                    new TableLayoutPanelCellPosition(zeroPos[1], zeroPos[0])
                    );

                games.DoSwap(labelCellPosition.Column, labelCellPosition.Row);
                this.labelCountOfUserSwappes.Text = string.Format("Кол-во перемещений: {0}", games.countOfSwappes);
                if (games.isGameEnd) {
                    endGame();
                }
            }
        }

        private void endGame() {
            MessageBox.Show(string.Format(
                "Поздравляем, Вы завершили игру!\n" + 
                "Кол-во перемещений пятнашек: {0}", games.countOfSwappes));
        }

        private void numericUDRandomizeField_ValueChanged(object sender, EventArgs e) {
            games = new Games((int) this.numericUDRandomizeField.Value);
            games.DoReset(fieldSize);
            clearTableLayoutPanel();
            setTableLayoutPanel(fieldSize);
        }

        private void toolStripNewGame_Click(object sender, EventArgs e) {
            games = new Games((int) this.numericUDRandomizeField.Value);
            games.DoReset(fieldSize);
            clearTableLayoutPanel();
            setTableLayoutPanel(fieldSize);
        }
    }
}
