﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabPyatnashki {
    class Games {
        public int[,] gameField { get; set; }
        private int countOfRandom { get; set; }
        public int countOfSwappes { get; set; }
        private int[,] endField { get; set; }
        public bool isGameEnd { get; private set; }

        public Games(int countOfRandom) {
            this.countOfRandom = countOfRandom;
        }

        public void DoReset(int fieldSize) {
            endField = fetchArray(fieldSize);
            gameField = fetchArray(fieldSize);

            countOfSwappes = 0;

            gameField = SwapField(gameField, countOfRandom);
        }

        // Метод, перемешивающий игровое поле
        public int[,] DoStartSwap(int zeroPosRow, int zeroPosCol, int[,] field) {
            Random random = new Random();
            int direction = random.Next(1, 4);
            int[,] resultField = field;

            try {
                switch (direction) {
                    case 1:
                        resultField[zeroPosRow, zeroPosCol] = resultField[zeroPosRow, zeroPosCol - 1];
                        resultField[zeroPosRow, zeroPosCol - 1] = 0;
                        break;
                    case 2:
                        resultField[zeroPosRow, zeroPosCol] = resultField[zeroPosRow - 1, zeroPosCol];
                        resultField[zeroPosRow - 1, zeroPosCol] = 0;
                        break;
                    case 3:
                        resultField[zeroPosRow, zeroPosCol] = resultField[zeroPosRow, zeroPosCol + 1];
                        resultField[zeroPosRow, zeroPosCol + 1] = 0;
                        break;
                    case 4:
                        resultField[zeroPosRow, zeroPosCol] = resultField[zeroPosRow + 1, zeroPosCol];
                        resultField[zeroPosRow + 1, zeroPosCol] = 0;
                        break;
                }
            } catch (IndexOutOfRangeException) {
                DoStartSwap(zeroPosRow, zeroPosCol, field);
            }
            return resultField;
        }

        
        public void DoSwap(int col, int row) {
            int[] zeroPos = findZero(gameField);
            int swappingElement = gameField[row, col];
            gameField[row, col] = 0;
            gameField[zeroPos[0], zeroPos[1]] = swappingElement;
            countOfSwappes++;

            if (checkEndOfGame()) {
                isGameEnd = true;
            }
            
        }

        private bool checkEndOfGame() {
            int fieldSize = (int) Math.Sqrt(gameField.Length);
            for(int i = 0; i < fieldSize; ++i) {
                for (int j = 0; j < fieldSize; ++j) {
                    if (gameField[i, j] != endField[i, j]) {
                        return false;
                    }
                }
            }
            return true;
        }
        
        private int[,] fetchArray(int fieldSize) {
            int counter = 1;
            int[,] resultArray = new int[fieldSize, fieldSize];
            for(int i = 0; i < fieldSize; ++i) {
                for (int j = 0; j < fieldSize; ++j, ++counter) {
                    resultArray[i, j] = counter;
                }
            }
            resultArray[fieldSize - 1, fieldSize - 1] = 0;
            return resultArray;
        }

        
        private int[,] SwapField(int[,] field, int countOfRandomSwappes) {
            int[,] resultField = field;
            int[] zeroPos = new int[2];
            for (int i = 0; i < countOfRandomSwappes; ++i) {
                zeroPos = findZero(resultField);
                resultField = DoStartSwap(zeroPos[0], zeroPos[1], resultField);
            }
            return resultField;
        }

        // Возвращает местоположение "нуля" (пустая клетка)
        // Первый элемент массива - строка, второй - столбец
        public int[] findZero(int[,] field) {
            int fieldSize = (int) Math.Sqrt(field.Length);
            for (int i = 0; i < fieldSize; ++i) {
                for (int j = 0; j < fieldSize; ++j) {
                    if (field[i, j] == 0) {
                        return new int[] { i, j };
                    }
                }
            }
            return null;
        }
    }
}
