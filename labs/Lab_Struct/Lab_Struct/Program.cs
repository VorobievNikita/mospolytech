﻿using System;
using System.Collections.Generic;

namespace Lab_Struct
{
    struct user
    {
        public string name;
        public string surname;
        public user(string name, string surname)
        {
            this.name = name;
            this.surname = surname;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Queue<user> Queue = new Queue<user>();
            Queue.Enqueue(new user("Ruslan", "Roflan"));
            Queue.Enqueue(new user("Vadim", "Roflan"));
            Queue.Enqueue(new user("Vadik", "Roflan"));

            while(Queue.Count > 0)
            {
                var x = Queue.Dequeue();
                Console.WriteLine($"{x.name} {x.surname}");
            }
            Console.ReadKey();
        }
    }
}
