﻿using System;

namespace Lab_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размерность массива: ");
            int[] a = new int[int.Parse(Console.ReadLine())];
            for(int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("Ввеlите " + i + " элемент массива: ");
                a[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Ваш массив: ");
            for(int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + " ");
            }
            a = null;
            GC.Collect();
            Console.ReadKey();
        }
    }
}
