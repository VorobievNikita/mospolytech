﻿using System;

namespace Lab_Polygon
{
    class Polygon
    {
        public virtual void Draw()
        {
            Console.WriteLine("Polygon.Draw");
        }
    }

    class Rectangle : Polygon
    {
        public override void Draw()
        {
            Console.WriteLine("Rectangle.Draw");
        }
    }

    class Triangle : Polygon
    {
        public override void Draw()
        {
            Console.WriteLine("Triangle.Draw");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Polygon polygon = new Polygon();
            polygon.Draw();
            polygon = new Rectangle();
            polygon.Draw();
            polygon = new Triangle();
            polygon.Draw();
            Console.ReadKey();
        }
    }
}
