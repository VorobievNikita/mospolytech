﻿using System;
using System.Collections.Generic;

namespace Lab_List
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> x = new List<int>() {1,2,3,4};
            x.Add(6);
            x.AddRange(new int[] { 7, 8, 9 });
            x.RemoveAt(1);
            foreach(int item in x)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }
}
