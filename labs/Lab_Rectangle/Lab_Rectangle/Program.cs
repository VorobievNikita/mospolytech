﻿using System;

namespace Lab_Rectangle
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите длину и ширину прямоугольника:");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            Rectangle x = new Rectangle(a,b);
            Console.WriteLine("Площадь - {0}", x.GetArea());
            Console.WriteLine("Периметр - {0}", x.GetP());
            Console.ReadKey();
        }
    }
}
