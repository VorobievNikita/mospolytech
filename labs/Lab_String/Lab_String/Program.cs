﻿using System;
using System.Text;

namespace Lab_String
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Вадим";
            string age = "16";
            string s1 = $"Моё имя - {name}, мне {age} лет!";
            Console.WriteLine(s1);
            s1 = String.Format("Моё имя - {0}, мне {1} лет!", name, age);
            Console.WriteLine(s1);
            string s2 = $"В Москве: {DateTime.Now:hh:mm}";
            Console.WriteLine(s2);

            StringBuilder str = new StringBuilder();
            str.Append("AAAAaaaaaaaaaaaaaaaaaaaaai ");
            str.Append("Become ");
            str.Append("so numb");
            string build = str.ToString();
            Console.WriteLine(build);

            Console.ReadKey();
        }
    }
}
