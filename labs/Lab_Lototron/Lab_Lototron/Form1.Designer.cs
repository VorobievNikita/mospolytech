﻿namespace Lab_Lototron
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.tcMenu = new System.Windows.Forms.TabControl();
            this.tbMenu = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.BuEsc = new System.Windows.Forms.Button();
            this.buGame = new System.Windows.Forms.Button();
            this.buScore = new System.Windows.Forms.Button();
            this.tbGame = new System.Windows.Forms.TabPage();
            this.laMessage = new System.Windows.Forms.Label();
            this.numBet = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.buPlay = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pic3 = new System.Windows.Forms.PictureBox();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buBack = new System.Windows.Forms.ToolStripButton();
            this.laScore = new System.Windows.Forms.ToolStripLabel();
            this.tbScore = new System.Windows.Forms.TabPage();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.buBack1 = new System.Windows.Forms.ToolStripButton();
            this.laBestScore = new System.Windows.Forms.Label();
            this.tcMenu.SuspendLayout();
            this.tbMenu.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tbGame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBet)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.tbScore.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMenu
            // 
            this.tcMenu.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tcMenu.Controls.Add(this.tbMenu);
            this.tcMenu.Controls.Add(this.tbGame);
            this.tcMenu.Controls.Add(this.tbScore);
            this.tcMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMenu.ItemSize = new System.Drawing.Size(0, 1);
            this.tcMenu.Location = new System.Drawing.Point(0, 0);
            this.tcMenu.Name = "tcMenu";
            this.tcMenu.SelectedIndex = 0;
            this.tcMenu.Size = new System.Drawing.Size(484, 461);
            this.tcMenu.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tcMenu.TabIndex = 0;
            this.tcMenu.TabStop = false;
            // 
            // tbMenu
            // 
            this.tbMenu.Controls.Add(this.tableLayoutPanel1);
            this.tbMenu.Location = new System.Drawing.Point(4, 5);
            this.tbMenu.Name = "tbMenu";
            this.tbMenu.Padding = new System.Windows.Forms.Padding(3);
            this.tbMenu.Size = new System.Drawing.Size(476, 452);
            this.tbMenu.TabIndex = 0;
            this.tbMenu.Text = "Menu";
            this.tbMenu.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.BuEsc, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.buGame, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buScore, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(470, 446);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // BuEsc
            // 
            this.BuEsc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BuEsc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BuEsc.Location = new System.Drawing.Point(10, 308);
            this.BuEsc.Margin = new System.Windows.Forms.Padding(10);
            this.BuEsc.Name = "BuEsc";
            this.BuEsc.Size = new System.Drawing.Size(450, 128);
            this.BuEsc.TabIndex = 3;
            this.BuEsc.Text = "Выход";
            this.BuEsc.UseVisualStyleBackColor = true;
            this.BuEsc.Click += new System.EventHandler(this.BuEsc_Click);
            // 
            // buGame
            // 
            this.buGame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buGame.Location = new System.Drawing.Point(10, 10);
            this.buGame.Margin = new System.Windows.Forms.Padding(10);
            this.buGame.Name = "buGame";
            this.buGame.Size = new System.Drawing.Size(450, 127);
            this.buGame.TabIndex = 1;
            this.buGame.Text = "Играть";
            this.buGame.UseVisualStyleBackColor = true;
            this.buGame.Click += new System.EventHandler(this.BuGame_Click);
            // 
            // buScore
            // 
            this.buScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buScore.Location = new System.Drawing.Point(10, 157);
            this.buScore.Margin = new System.Windows.Forms.Padding(10);
            this.buScore.Name = "buScore";
            this.buScore.Size = new System.Drawing.Size(450, 131);
            this.buScore.TabIndex = 2;
            this.buScore.Text = "Рекорды";
            this.buScore.UseVisualStyleBackColor = true;
            this.buScore.Click += new System.EventHandler(this.BuScore_Click);
            // 
            // tbGame
            // 
            this.tbGame.Controls.Add(this.laMessage);
            this.tbGame.Controls.Add(this.numBet);
            this.tbGame.Controls.Add(this.label1);
            this.tbGame.Controls.Add(this.buPlay);
            this.tbGame.Controls.Add(this.tableLayoutPanel2);
            this.tbGame.Controls.Add(this.toolStrip1);
            this.tbGame.Location = new System.Drawing.Point(4, 5);
            this.tbGame.Name = "tbGame";
            this.tbGame.Padding = new System.Windows.Forms.Padding(3);
            this.tbGame.Size = new System.Drawing.Size(476, 452);
            this.tbGame.TabIndex = 1;
            this.tbGame.Text = "Game";
            this.tbGame.UseVisualStyleBackColor = true;
            // 
            // laMessage
            // 
            this.laMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laMessage.AutoSize = true;
            this.laMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laMessage.Location = new System.Drawing.Point(164, 274);
            this.laMessage.Name = "laMessage";
            this.laMessage.Size = new System.Drawing.Size(150, 29);
            this.laMessage.TabIndex = 5;
            this.laMessage.Text = "Сообщение";
            // 
            // numBet
            // 
            this.numBet.Location = new System.Drawing.Point(172, 251);
            this.numBet.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numBet.Name = "numBet";
            this.numBet.Size = new System.Drawing.Size(120, 20);
            this.numBet.TabIndex = 4;
            this.numBet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numBet.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(200, 231);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ставка:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buPlay
            // 
            this.buPlay.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buPlay.Location = new System.Drawing.Point(3, 399);
            this.buPlay.Name = "buPlay";
            this.buPlay.Size = new System.Drawing.Size(470, 50);
            this.buPlay.TabIndex = 2;
            this.buPlay.Text = "Крутить";
            this.buPlay.UseVisualStyleBackColor = true;
            this.buPlay.Click += new System.EventHandler(this.BuPlay_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.Controls.Add(this.pic3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(470, 200);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // pic3
            // 
            this.pic3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic3.Location = new System.Drawing.Point(317, 3);
            this.pic3.Name = "pic3";
            this.pic3.Size = new System.Drawing.Size(150, 194);
            this.pic3.TabIndex = 7;
            this.pic3.TabStop = false;
            // 
            // pic1
            // 
            this.pic1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic1.Location = new System.Drawing.Point(3, 3);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(149, 194);
            this.pic1.TabIndex = 5;
            this.pic1.TabStop = false;
            // 
            // pic2
            // 
            this.pic2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic2.Location = new System.Drawing.Point(158, 3);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(153, 194);
            this.pic2.TabIndex = 6;
            this.pic2.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buBack,
            this.laScore});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(470, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buBack
            // 
            this.buBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buBack.Image = ((System.Drawing.Image)(resources.GetObject("buBack.Image")));
            this.buBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(53, 22);
            this.buBack.Text = "В меню";
            this.buBack.Click += new System.EventHandler(this.BuBack_Click);
            // 
            // laScore
            // 
            this.laScore.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.laScore.Name = "laScore";
            this.laScore.Size = new System.Drawing.Size(57, 22);
            this.laScore.Text = "Счет: 100";
            // 
            // tbScore
            // 
            this.tbScore.Controls.Add(this.laBestScore);
            this.tbScore.Controls.Add(this.toolStrip2);
            this.tbScore.Location = new System.Drawing.Point(4, 5);
            this.tbScore.Name = "tbScore";
            this.tbScore.Padding = new System.Windows.Forms.Padding(3);
            this.tbScore.Size = new System.Drawing.Size(476, 452);
            this.tbScore.TabIndex = 2;
            this.tbScore.Text = "Score";
            this.tbScore.UseVisualStyleBackColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "100175077592750496.jpg");
            this.imageList1.Images.SetKeyName(1, "41RU74b+K8L.jpg");
            this.imageList1.Images.SetKeyName(2, "14982944.jpg");
            this.imageList1.Images.SetKeyName(3, "коля.png");
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buBack1});
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(470, 25);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // buBack1
            // 
            this.buBack1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buBack1.Image = ((System.Drawing.Image)(resources.GetObject("buBack1.Image")));
            this.buBack1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buBack1.Name = "buBack1";
            this.buBack1.Size = new System.Drawing.Size(43, 22);
            this.buBack1.Text = "Назад";
            this.buBack1.Click += new System.EventHandler(this.BuBack_Click);
            // 
            // laBestScore
            // 
            this.laBestScore.AutoSize = true;
            this.laBestScore.Dock = System.Windows.Forms.DockStyle.Top;
            this.laBestScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laBestScore.Location = new System.Drawing.Point(3, 28);
            this.laBestScore.Name = "laBestScore";
            this.laBestScore.Size = new System.Drawing.Size(251, 31);
            this.laBestScore.TabIndex = 1;
            this.laBestScore.Text = "Лучший счет: 100";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.tcMenu);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "fm";
            this.Text = "Lab_Lototron";
            this.tcMenu.ResumeLayout(false);
            this.tbMenu.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tbGame.ResumeLayout(false);
            this.tbGame.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBet)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tbScore.ResumeLayout(false);
            this.tbScore.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMenu;
        private System.Windows.Forms.TabPage tbMenu;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button BuEsc;
        private System.Windows.Forms.Button buGame;
        private System.Windows.Forms.Button buScore;
        private System.Windows.Forms.TabPage tbGame;
        private System.Windows.Forms.Label laMessage;
        private System.Windows.Forms.NumericUpDown numBet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pic3;
        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.PictureBox pic2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buBack;
        private System.Windows.Forms.ToolStripLabel laScore;
        private System.Windows.Forms.TabPage tbScore;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton buBack1;
        private System.Windows.Forms.Label laBestScore;
    }
}

