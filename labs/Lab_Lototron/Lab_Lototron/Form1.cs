﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Lab_Lototron
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            StreamReader sr = new StreamReader("score.txt", System.Text.Encoding.Default);
            laBestScore.Text = "Лучший счет: " + sr.ReadLine();
            sr.Close();
        }

        private void BuPlay_Click(object sender, EventArgs e)
        {
            pic1.Visible = true;
            pic2.Visible = true;
            pic3.Visible = true;
            Random rand = new Random();
            int o;
            o = rand.Next(0, imageList1.Images.Count);
            pic1.Image = imageList1.Images[o];
            pic1.Tag = o;
            o = rand.Next(0, imageList1.Images.Count);
            pic2.Image = imageList1.Images[o];
            pic2.Tag = o;
            o = rand.Next(0, imageList1.Images.Count);
            pic3.Image = imageList1.Images[o];
            pic3.Tag = o;
            int score = Convert.ToInt32(laScore.Text.Substring(6)) - Convert.ToInt32(numBet.Value);
            laScore.Text = "Счет: " + score.ToString();
            if((pic1.Tag.ToString() == pic2.Tag.ToString()) && (pic2.Tag.ToString() == pic3.Tag.ToString()))
            {
                laScore.Text = "Счет: " + (score + Convert.ToInt32(numBet.Value)*2).ToString();
                score = Convert.ToInt32(laScore.Text.Substring(6));
                StreamWriter sw = new StreamWriter("score.txt", false, System.Text.Encoding.Default);
                sw.Close();
                StreamReader sr = new StreamReader("score.txt", System.Text.Encoding.Default);
                if (score>Convert.ToInt32(sr.ReadLine()))
                {
                    sr.Close();
                    sw = new StreamWriter("score.txt", false, System.Text.Encoding.Default);
                    sw.WriteLine(score.ToString());
                    sw.Close();
                    sr = new StreamReader("score.txt", System.Text.Encoding.Default);
                    laBestScore.Text = "Лучший счет: " + sr.ReadLine();
                    sr.Close();
                    
                }
            }
            if(score<=0)
            {
                MessageBox.Show("Вы проиграли!");
                tcMenu.SelectedTab = tbMenu;
                laScore.Text = "Счет: 100";
                pic1.Visible = false;
                pic2.Visible = false;
                pic3.Visible = false;
                numBet.Maximum = 100;
            }
            else
            {
                numBet.Maximum = score;
            }
        }

        private void BuGame_Click(object sender, EventArgs e)
        {
            tcMenu.SelectedTab = tbGame;
        }

        private void BuEsc_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BuScore_Click(object sender, EventArgs e)
        {
            tcMenu.SelectedTab = tbScore;
        }

        private void BuBack_Click(object sender, EventArgs e)
        {
            tcMenu.SelectedTab = tbMenu;
        }
    }
}
