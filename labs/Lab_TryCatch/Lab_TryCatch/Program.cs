﻿using System;

namespace Lab_TryCatch
{
    class Program
    {
        static void Main(string[] args)
        {
            int y = 0;
            try
            {
                int[] z = {1,2,3};
                for (int i = 0; i<4;i++)
                {
                    int a = z[i];
                    Console.WriteLine(z[i]);
                }
                int x = 4 / y;
            }
            catch(DivideByZeroException dze)
            {
                Console.WriteLine("Ошибка деления на ноль - {0}", dze.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Ошибка - {0}", ex.Message);
            }
            finally
            {
                y = 123;
            }
            Console.WriteLine(y);
            Console.ReadKey();
        }
    }
}
