﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labMonster
{
    public partial class fm : Form
    {
        public int HeroSpeed { get; set; }
        Timer tm;
        int hp = 100;
        public fm()
        {
            InitializeComponent();
            tm = new Timer();
            tm.Interval = 500;
            tm.Tick += Tm_Tick;
            tm.Enabled = true;
            HeroSpeed = 1;
            this.KeyDown += Fm_KeyDown;
            this.KeyUp += Fm_KeyUp;
        }

        private void Tm_Tick(object sender, EventArgs e)
        {
            int x1;
            int y1;
            int step = 2;
            foreach(Control c in fm.ActiveForm.Controls)
            {
                if (c.Name == "laHP")
                {
                    ;
                }
                else
                {

                    PictureBox c1 = c as PictureBox;
                    if (c1.Name != "picHero")
                    {
                        x1 = c1.Location.X;
                        y1 = c1.Location.Y;
                        if (picHero.Location.X >= x1)
                        {
                            x1 = x1 + step;
                            c1.Location = new Point(x1, y1);
                        }
                        else
                        {
                            x1 = x1 - step;
                            c1.Location = new Point(x1, y1);
                        }
                        if (picHero.Location.Y >= y1)
                        {
                            y1 = y1 + step;
                            c1.Location = new Point(x1, y1);
                        }
                        else
                        {
                            y1 = y1 - step;
                            c1.Location = new Point(x1, y1);
                        }
                        if ((Math.Abs(picHero.Location.X - x1) <= 50) && (Math.Abs(picHero.Location.Y - y1) <= 50))
                        {
                            hp = hp - 5;
                        }
                        step++;
                    }
                }
            }
            if(hp <= 0)
            {
                tm.Enabled = false;
                hp = 100;
                MessageBox.Show("Вы проиграли!");
                picHero.Location = new Point(50, 50);
                picEn1.Location = new Point(200, 200);
                picEn2.Location = new Point(300, 300);
                picEn3.Location = new Point(400, 400);
                tm.Enabled = true;
            }
            laHP.Text = hp.ToString();
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            HeroSpeed = 1;
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            int x = picHero.Location.X;
            int y = picHero.Location.Y;
            if(HeroSpeed<7)
            {
                HeroSpeed++;
            }
            if ((e.KeyCode == Keys.Left) && (x- HeroSpeed >= 0))
            {
                picHero.Location = new Point(x - HeroSpeed, y);
            }
            if ((e.KeyCode == Keys.Right) && (x + HeroSpeed <= fm.ActiveForm.ClientSize.Width - 100))
            {
                picHero.Location = new Point(x + HeroSpeed, y);
            }
            if ((e.KeyCode == Keys.Up) && (y - HeroSpeed >= 0))
            {
                picHero.Location = new Point(x, y - HeroSpeed);
            }
            if ((e.KeyCode == Keys.Down) && (y+ HeroSpeed <= fm.ActiveForm.ClientSize.Height - 100))
            {
                picHero.Location = new Point(x, y + HeroSpeed);
            }
        }
    }
}
