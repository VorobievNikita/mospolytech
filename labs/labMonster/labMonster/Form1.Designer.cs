﻿namespace labMonster
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.picHero = new System.Windows.Forms.PictureBox();
            this.picEn1 = new System.Windows.Forms.PictureBox();
            this.picEn2 = new System.Windows.Forms.PictureBox();
            this.picEn3 = new System.Windows.Forms.PictureBox();
            this.laHP = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picHero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEn2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEn3)).BeginInit();
            this.SuspendLayout();
            // 
            // picHero
            // 
            this.picHero.Image = global::labMonster.Properties.Resources.rambo_PNG18;
            this.picHero.Location = new System.Drawing.Point(57, 60);
            this.picHero.Name = "picHero";
            this.picHero.Size = new System.Drawing.Size(100, 100);
            this.picHero.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHero.TabIndex = 0;
            this.picHero.TabStop = false;
            // 
            // picEn1
            // 
            this.picEn1.Image = global::labMonster.Properties.Resources._12;
            this.picEn1.Location = new System.Drawing.Point(285, 60);
            this.picEn1.Name = "picEn1";
            this.picEn1.Size = new System.Drawing.Size(70, 70);
            this.picEn1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picEn1.TabIndex = 1;
            this.picEn1.TabStop = false;
            // 
            // picEn2
            // 
            this.picEn2.Image = global::labMonster.Properties.Resources._12;
            this.picEn2.Location = new System.Drawing.Point(304, 151);
            this.picEn2.Name = "picEn2";
            this.picEn2.Size = new System.Drawing.Size(70, 70);
            this.picEn2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picEn2.TabIndex = 2;
            this.picEn2.TabStop = false;
            // 
            // picEn3
            // 
            this.picEn3.Image = global::labMonster.Properties.Resources._12;
            this.picEn3.Location = new System.Drawing.Point(304, 268);
            this.picEn3.Name = "picEn3";
            this.picEn3.Size = new System.Drawing.Size(70, 70);
            this.picEn3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picEn3.TabIndex = 3;
            this.picEn3.TabStop = false;
            // 
            // laHP
            // 
            this.laHP.AutoSize = true;
            this.laHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laHP.ForeColor = System.Drawing.Color.Red;
            this.laHP.Location = new System.Drawing.Point(13, 13);
            this.laHP.Name = "laHP";
            this.laHP.Size = new System.Drawing.Size(39, 20);
            this.laHP.TabIndex = 4;
            this.laHP.Text = "100";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.laHP);
            this.Controls.Add(this.picEn3);
            this.Controls.Add(this.picEn2);
            this.Controls.Add(this.picEn1);
            this.Controls.Add(this.picHero);
            this.Name = "fm";
            this.Text = "labMonster";
            ((System.ComponentModel.ISupportInitialize)(this.picHero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEn2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEn3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picHero;
        private System.Windows.Forms.PictureBox picEn1;
        private System.Windows.Forms.PictureBox picEn2;
        private System.Windows.Forms.PictureBox picEn3;
        private System.Windows.Forms.Label laHP;
    }
}

