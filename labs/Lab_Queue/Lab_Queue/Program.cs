﻿using System;
using System.Collections;

namespace Lab_Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue x = new Queue();
            x.Enqueue(6);
            x.Enqueue(4);
            x.Enqueue(1);
            x.Enqueue(5);
            Console.WriteLine(x.Peek());
            Console.WriteLine("----");
            while(x.Count>0)
            {
                int v = (int)x.Dequeue();
                Console.WriteLine(v);
            }
            Console.ReadKey();
        }
    }
}
