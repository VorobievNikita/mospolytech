﻿using System;
using System.Collections.Generic;

namespace Lab_Dict
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>();
            for(int i = 0; i<5;i++)
            {
                x.Add(i, Convert.ToString(i));
            }
            foreach(KeyValuePair<int,string> keyvalue in x)
            {
                Console.WriteLine(keyvalue.Key + " - " + keyvalue.Value);
            }
            Console.ReadKey();
        }
    }
}
