﻿namespace Lab_Poesia2
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.tbMain = new System.Windows.Forms.TextBox();
            this.tbChange = new System.Windows.Forms.TextBox();
            this.buOpen = new System.Windows.Forms.Button();
            this.buFLetter = new System.Windows.Forms.Button();
            this.buFWord = new System.Windows.Forms.Button();
            this.buThr = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buZoomOut = new System.Windows.Forms.ToolStripButton();
            this.buZoomIn = new System.Windows.Forms.ToolStripButton();
            this.buAbout = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbMain
            // 
            this.tbMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbMain.Location = new System.Drawing.Point(12, 28);
            this.tbMain.Multiline = true;
            this.tbMain.Name = "tbMain";
            this.tbMain.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbMain.Size = new System.Drawing.Size(250, 421);
            this.tbMain.TabIndex = 0;
            this.tbMain.Text = resources.GetString("tbMain.Text");
            // 
            // tbChange
            // 
            this.tbChange.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbChange.Location = new System.Drawing.Point(422, 28);
            this.tbChange.Multiline = true;
            this.tbChange.Name = "tbChange";
            this.tbChange.ReadOnly = true;
            this.tbChange.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbChange.Size = new System.Drawing.Size(250, 421);
            this.tbChange.TabIndex = 1;
            // 
            // buOpen
            // 
            this.buOpen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buOpen.Location = new System.Drawing.Point(283, 28);
            this.buOpen.Name = "buOpen";
            this.buOpen.Size = new System.Drawing.Size(120, 41);
            this.buOpen.TabIndex = 2;
            this.buOpen.Text = "Окрыть...";
            this.buOpen.UseVisualStyleBackColor = true;
            // 
            // buFLetter
            // 
            this.buFLetter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buFLetter.Location = new System.Drawing.Point(283, 169);
            this.buFLetter.Name = "buFLetter";
            this.buFLetter.Size = new System.Drawing.Size(120, 41);
            this.buFLetter.TabIndex = 3;
            this.buFLetter.Text = "Первая буква слова";
            this.buFLetter.UseVisualStyleBackColor = true;
            // 
            // buFWord
            // 
            this.buFWord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buFWord.Location = new System.Drawing.Point(283, 122);
            this.buFWord.Name = "buFWord";
            this.buFWord.Size = new System.Drawing.Size(120, 41);
            this.buFWord.TabIndex = 4;
            this.buFWord.Text = "Первое слово";
            this.buFWord.UseVisualStyleBackColor = true;
            // 
            // buThr
            // 
            this.buThr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buThr.Location = new System.Drawing.Point(283, 75);
            this.buThr.Name = "buThr";
            this.buThr.Size = new System.Drawing.Size(120, 41);
            this.buThr.TabIndex = 5;
            this.buThr.Text = "Через строку";
            this.buThr.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buZoomOut,
            this.buZoomIn,
            this.buAbout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(684, 25);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buZoomOut
            // 
            this.buZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("buZoomOut.Image")));
            this.buZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buZoomOut.Name = "buZoomOut";
            this.buZoomOut.Size = new System.Drawing.Size(63, 22);
            this.buZoomOut.Text = "ZoomOut";
            // 
            // buZoomIn
            // 
            this.buZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("buZoomIn.Image")));
            this.buZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buZoomIn.Name = "buZoomIn";
            this.buZoomIn.Size = new System.Drawing.Size(53, 22);
            this.buZoomIn.Text = "ZoomIn";
            // 
            // buAbout
            // 
            this.buAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buAbout.Image = ((System.Drawing.Image)(resources.GetObject("buAbout.Image")));
            this.buAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buAbout.Name = "buAbout";
            this.buAbout.Size = new System.Drawing.Size(44, 22);
            this.buAbout.Text = "About";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 461);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.buThr);
            this.Controls.Add(this.buFWord);
            this.Controls.Add(this.buFLetter);
            this.Controls.Add(this.buOpen);
            this.Controls.Add(this.tbChange);
            this.Controls.Add(this.tbMain);
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "fm";
            this.Text = "Lab_Poesia2";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbMain;
        private System.Windows.Forms.TextBox tbChange;
        private System.Windows.Forms.Button buOpen;
        private System.Windows.Forms.Button buFLetter;
        private System.Windows.Forms.Button buFWord;
        private System.Windows.Forms.Button buThr;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buZoomOut;
        private System.Windows.Forms.ToolStripButton buZoomIn;
        private System.Windows.Forms.ToolStripButton buAbout;
    }
}

