﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilsStr;

namespace Lab_Poesia2
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            buZoomOut.Click += BuZoomOut_Click;
            buZoomIn.Click += BuZoomIn_Click;
            buAbout.Click += BuAbout_Click;
            buThr.Click += BuThr_Click;
            buFWord.Click += BuFWord_Click;
            buFLetter.Click += BuFLetter_Click;
            buOpen.Click += BuOpen_Click;
        }

        private void BuOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            string filename = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = openFileDialog1.FileName;
            }
            tbMain.Text = System.IO.File.ReadAllText(filename, Encoding.Default)/*.Replace("\n", " ")*/;

        }

        private void BuFLetter_Click(object sender, EventArgs e)
        {
            tbChange.Lines = UtilsStr.UtilsStr.FirstLetter(tbMain.Lines);
        }

        private void BuFWord_Click(object sender, EventArgs e)
        {
            tbChange.Lines = UtilsStr.UtilsStr.FirstLine(tbMain.Lines);
        }

        private void BuThr_Click(object sender, EventArgs e)
        {
            tbChange.Lines = UtilsStr.UtilsStr.ThroughtLine(tbMain.Lines);
        }

        private void BuAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Lab_Poesia2");
        }

        private void BuZoomIn_Click(object sender, EventArgs e)
        {
            float x = tbMain.Font.Size;
            x += 1;
            tbMain.Font = new Font(tbMain.Font.FontFamily, x);
            tbChange.Font = new Font(tbChange.Font.FontFamily, x);
        }

        private void BuZoomOut_Click(object sender, EventArgs e)
        {
            float x = tbMain.Font.Size;
            x -= 1;
            tbMain.Font = new Font(tbMain.Font.FontFamily, x);
            tbChange.Font = new Font(tbChange.Font.FontFamily, x);
        }
    }
}
