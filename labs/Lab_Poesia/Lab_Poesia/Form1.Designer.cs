﻿namespace Lab_Poesia
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buZoomOut = new System.Windows.Forms.ToolStripButton();
            this.buZoomIn = new System.Windows.Forms.ToolStripButton();
            this.buAbout = new System.Windows.Forms.ToolStripButton();
            this.tc = new System.Windows.Forms.TabControl();
            this.tp1 = new System.Windows.Forms.TabPage();
            this.tb1 = new System.Windows.Forms.TextBox();
            this.tp2 = new System.Windows.Forms.TabPage();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.tp3 = new System.Windows.Forms.TabPage();
            this.tb3 = new System.Windows.Forms.TextBox();
            this.tp4 = new System.Windows.Forms.TabPage();
            this.tb4 = new System.Windows.Forms.TextBox();
            this.toolStrip1.SuspendLayout();
            this.tc.SuspendLayout();
            this.tp1.SuspendLayout();
            this.tp2.SuspendLayout();
            this.tp3.SuspendLayout();
            this.tp4.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buZoomOut,
            this.buZoomIn,
            this.buAbout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(484, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buZoomOut
            // 
            this.buZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("buZoomOut.Image")));
            this.buZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buZoomOut.Name = "buZoomOut";
            this.buZoomOut.Size = new System.Drawing.Size(63, 22);
            this.buZoomOut.Text = "ZoomOut";
            // 
            // buZoomIn
            // 
            this.buZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("buZoomIn.Image")));
            this.buZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buZoomIn.Name = "buZoomIn";
            this.buZoomIn.Size = new System.Drawing.Size(53, 22);
            this.buZoomIn.Text = "ZoomIn";
            // 
            // buAbout
            // 
            this.buAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buAbout.Image = ((System.Drawing.Image)(resources.GetObject("buAbout.Image")));
            this.buAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buAbout.Name = "buAbout";
            this.buAbout.Size = new System.Drawing.Size(44, 22);
            this.buAbout.Text = "About";
            // 
            // tc
            // 
            this.tc.Controls.Add(this.tp1);
            this.tc.Controls.Add(this.tp2);
            this.tc.Controls.Add(this.tp3);
            this.tc.Controls.Add(this.tp4);
            this.tc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc.Location = new System.Drawing.Point(0, 25);
            this.tc.Name = "tc";
            this.tc.SelectedIndex = 0;
            this.tc.Size = new System.Drawing.Size(484, 436);
            this.tc.TabIndex = 1;
            // 
            // tp1
            // 
            this.tp1.Controls.Add(this.tb1);
            this.tp1.Location = new System.Drawing.Point(4, 22);
            this.tp1.Name = "tp1";
            this.tp1.Padding = new System.Windows.Forms.Padding(3);
            this.tp1.Size = new System.Drawing.Size(476, 410);
            this.tp1.TabIndex = 0;
            this.tp1.Text = "Полностью";
            this.tp1.UseVisualStyleBackColor = true;
            // 
            // tb1
            // 
            this.tb1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb1.Location = new System.Drawing.Point(3, 3);
            this.tb1.Multiline = true;
            this.tb1.Name = "tb1";
            this.tb1.ReadOnly = true;
            this.tb1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb1.Size = new System.Drawing.Size(470, 404);
            this.tb1.TabIndex = 0;
            this.tb1.Text = resources.GetString("tb1.Text");
            // 
            // tp2
            // 
            this.tp2.Controls.Add(this.tb2);
            this.tp2.Location = new System.Drawing.Point(4, 22);
            this.tp2.Name = "tp2";
            this.tp2.Padding = new System.Windows.Forms.Padding(3);
            this.tp2.Size = new System.Drawing.Size(476, 410);
            this.tp2.TabIndex = 1;
            this.tp2.Text = "Через строчку";
            this.tp2.UseVisualStyleBackColor = true;
            // 
            // tb2
            // 
            this.tb2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb2.Location = new System.Drawing.Point(3, 3);
            this.tb2.Multiline = true;
            this.tb2.Name = "tb2";
            this.tb2.ReadOnly = true;
            this.tb2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb2.Size = new System.Drawing.Size(470, 404);
            this.tb2.TabIndex = 1;
            // 
            // tp3
            // 
            this.tp3.Controls.Add(this.tb3);
            this.tp3.Location = new System.Drawing.Point(4, 22);
            this.tp3.Name = "tp3";
            this.tp3.Padding = new System.Windows.Forms.Padding(3);
            this.tp3.Size = new System.Drawing.Size(476, 410);
            this.tp3.TabIndex = 2;
            this.tp3.Text = "Первые слова";
            this.tp3.UseVisualStyleBackColor = true;
            // 
            // tb3
            // 
            this.tb3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb3.Location = new System.Drawing.Point(3, 3);
            this.tb3.Multiline = true;
            this.tb3.Name = "tb3";
            this.tb3.ReadOnly = true;
            this.tb3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb3.Size = new System.Drawing.Size(470, 404);
            this.tb3.TabIndex = 2;
            // 
            // tp4
            // 
            this.tp4.Controls.Add(this.tb4);
            this.tp4.Location = new System.Drawing.Point(4, 22);
            this.tp4.Name = "tp4";
            this.tp4.Padding = new System.Windows.Forms.Padding(3);
            this.tp4.Size = new System.Drawing.Size(476, 410);
            this.tp4.TabIndex = 3;
            this.tp4.Text = "Первые буквы";
            this.tp4.UseVisualStyleBackColor = true;
            // 
            // tb4
            // 
            this.tb4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb4.Location = new System.Drawing.Point(3, 3);
            this.tb4.Multiline = true;
            this.tb4.Name = "tb4";
            this.tb4.ReadOnly = true;
            this.tb4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb4.Size = new System.Drawing.Size(470, 404);
            this.tb4.TabIndex = 3;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.tc);
            this.Controls.Add(this.toolStrip1);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "fm";
            this.Text = "Lab_Poesia";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tc.ResumeLayout(false);
            this.tp1.ResumeLayout(false);
            this.tp1.PerformLayout();
            this.tp2.ResumeLayout(false);
            this.tp2.PerformLayout();
            this.tp3.ResumeLayout(false);
            this.tp3.PerformLayout();
            this.tp4.ResumeLayout(false);
            this.tp4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buZoomOut;
        private System.Windows.Forms.ToolStripButton buZoomIn;
        private System.Windows.Forms.ToolStripButton buAbout;
        private System.Windows.Forms.TabControl tc;
        private System.Windows.Forms.TabPage tp1;
        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.TabPage tp2;
        private System.Windows.Forms.TabPage tp3;
        private System.Windows.Forms.TextBox tb2;
        private System.Windows.Forms.TextBox tb3;
        private System.Windows.Forms.TabPage tp4;
        private System.Windows.Forms.TextBox tb4;
    }
}

