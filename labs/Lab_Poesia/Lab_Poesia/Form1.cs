﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilsStr;

namespace Lab_Poesia
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            tc.TabIndex = 0;
            tb2.Lines = UtilsStr.UtilsStr.ThroughtLine(tb1.Lines);
            tb3.Lines = UtilsStr.UtilsStr.FirstLine(tb1.Lines);
            tb4.Lines = UtilsStr.UtilsStr.FirstLetter(tb1.Lines);
            buZoomOut.Click += BuZoomOut_Click;
            buZoomIn.Click += BuZoomIn_Click;
            buAbout.Click += BuAbout_Click;
        }

        private void BuAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Lab_Poesia");
        }

        private void BuZoomIn_Click(object sender, EventArgs e)
        {
            float x = tb1.Font.Size;
            x += 1;
            tb1.Font = new Font(tb1.Font.FontFamily, x);
            tb2.Font = new Font(tb2.Font.FontFamily, x);
            tb3.Font = new Font(tb3.Font.FontFamily, x);
        }

        private void BuZoomOut_Click(object sender, EventArgs e)
        {
            float x = tb1.Font.Size;
            x -= 1;
            tb1.Font = new Font(tb1.Font.FontFamily, x);
            tb2.Font = new Font(tb2.Font.FontFamily, x);
            tb3.Font = new Font(tb3.Font.FontFamily, x);
        }
    }
}
