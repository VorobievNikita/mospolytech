﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilsStr
{
    public class UtilsStr
    {
        public static string[] ThroughtLine(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            for(int i = 0; i< v.Length;i++)
            {
                x.Clear();
                x.Append(v[i]);
                if(i % 2 == 1)
                {
                    for(int j = 0; j < x.Length;j++)
                    {
                        if(x[j] != ' ')
                        {
                            x[j] = 'x';
                        }
                    }
                }
                xResult[i] = x.ToString();
            }
            return xResult;
        }
        public static string[] FirstLine(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            bool flag;
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                flag = false;
                
                for (int j = 0; j < x.Length; j++)
                {
                    if (flag && (x[j] != ' '))
                    {
                        x[j] = 'x';
                    }
                    if ((!flag) && (x[j] == ' '))
                    {
                        flag = true;
                    }
                }
                xResult[i] = x.ToString();
            }
            return xResult;
        }
        public static string[] FirstLetter(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            bool flag;
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                flag = false;
                for (int j = 0; j < x.Length; j++)
                {
                    if (x[j] != ' ')
                    {
                        if (flag)
                        {
                            x[j] = 'x';
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                    if(x[j] == ' ')
                    {
                        flag = false;
                    }
                }
                xResult[i] = x.ToString();
            }
            return xResult;
        }
    }
}
