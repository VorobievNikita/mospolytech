﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_TrainerCount
{
    class Game
    {
        public int CountCorrect { get; protected set; }
        public int CountWrong { get; protected set; }
        public bool AnswerCorrect { get; protected set; }
        public string CodeText { get; protected set; }
        public int Level { get; protected set; }
        public int Try { get; protected set; }
        public event EventHandler Change;

        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            Level = 1;
            Try = 3;
            DoContinue();
        }
        public void DoContinue()
        {
            Random rand = new Random();

            int x = rand.Next(19+Level);
            int y = rand.Next(19 + Level);

            int res = x + y;

            int newres;

            if(rand.Next(2) == 1)
            {
                newres = res;
            }
            else
            {
                if(rand.Next(2) == 1)
                {
                    newres = res + 1;
                }
                else
                {
                    newres = res - 1;
                }
            }
            AnswerCorrect = (res == newres);
            CodeText = x.ToString() + " + " + y.ToString() + " = " + newres.ToString();
            Change?.Invoke(this, EventArgs.Empty);
        }
        public void DoAnswer(bool v)
        {
            if(v == AnswerCorrect)
            {
                CountCorrect++;
                Level++;
            }
            else
            {
                CountWrong++;
                if(Level > 1)
                {
                    Level--;
                }
                Try--;
                if(Try < 1)
                {
                    DoReset();
                }
            }
            DoContinue();
        }
    }
}
