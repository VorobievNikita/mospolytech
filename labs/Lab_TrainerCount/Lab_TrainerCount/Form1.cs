﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_TrainerCount
{
    public partial class fm : Form
    {
        Game game;
        public fm()
        {
            InitializeComponent();
            game = new Game();
            game.Change += Event_Change;
            game.DoReset();
        }
        private void Event_Change(object sender, EventArgs e)
        {
            laCalc.Text = game.CodeText;
            laCor.Text = "Верно = " + game.CountCorrect;
            laIrc.Text = "Неверно = " + game.CountWrong;
            laLvl.Text = "Верно? (Уровень " + game.Level + ") Жизни: " + game.Try;
        }

        private void BuYes_Click(object sender, EventArgs e)
        {
            game.DoAnswer(true);
        }

        private void BuNo_Click(object sender, EventArgs e)
        {
            game.DoAnswer(false);
        }
    }
}
