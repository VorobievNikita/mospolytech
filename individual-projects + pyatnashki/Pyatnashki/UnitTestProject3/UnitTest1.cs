using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary1;

namespace UnitTestProject3
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CreateField_Test()
        {
            int w = 4;
            int h = 4;
            int[,] field;
            int[,] result_field;
            int value;
            Class1 c;

            result_field = new int[w, h];
            field = new int[w, h];
            value = 0;
            c = new Class1(w, h, 20, 1);
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result_field[i, j] = value;
                    value++;
                }
            }
            c.CreateField(field, w, h);
            Assert.AreEqual(field, result_field);
        }
        [TestMethod]
        public void Move_Test()
        {
            int w = 4;
            int h = 4;
            int[,] result_field;
            int value;
            Class1 c;

            result_field = new int[w, h];
            value = 0;
            c = new Class1(w, h, 20, 1);
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result_field[i, j] = value;
                    value++;
                }
            }
            c.Move(result_field, 0);
            Assert.AreEqual(result_field[0,1], 0);
        }
        [TestMethod]
        public void FindZero_Test()
        {
            int w = 4;
            int h = 4;
            int[,] result_field;
            int value;
            Class1 c;
            int[] result;

            result = new int[2];
            result_field = new int[w, h];
            value = 0;
            result[0] = 0;
            result[1] = 0;
            c = new Class1(w, h, 20, 1);
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result_field[i, j] = value;
                    value++;
                }
            }
            Assert.AreEqual(c.FindZero(result_field), result);
        }
        [TestMethod]
        public void Validate_Test()
        {
            int w = 4;
            int h = 4;
            int[,] result_field;
            int value;
            Class1 c;
            int[] result;

            result = new int[2];
            result_field = new int[w, h];
            value = 0;
            result[0] = 0;
            result[1] = 0;
            c = new Class1(w, h, 20, 1);
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result_field[i, j] = value;
                    value++;
                }
            }
            Assert.AreEqual(c.Validate(result_field, w, h), true);
        }
    }
}
