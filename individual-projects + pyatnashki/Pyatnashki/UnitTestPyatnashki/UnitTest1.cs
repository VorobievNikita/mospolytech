﻿using ClassLibrary1;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UnitTestPyatnashki
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1CreateField_Test()
        {
            int width_val = 4;
            int height_val = 4;
            int[,] field;
            int[,] result_field;
            int value;

            value = 0;
            result_field = new int[width_val, height_val];
            for (int i = 0; i < width_val; i++)
            {
                for (int j = 0; j < height_val; j++)
                {
                    result_field[i, j] = value;
                    value++;
                }
            }

            CreateField();
        }
    }
}

/*
         private void CreateField()
        {
            int value;

            field = new int[width_val, height_val];
            value = 0;
            for (int i = 0; i < width_val; i++)
            {
                for (int j = 0; j < height_val; j++)
                {
                    field[i, j] = value;
                    value++;
                }
            }
        }
        private void Shafl()
        {
            int point;
            rnd = new Random();

            for (int i = 0; i < shafls; i++)
            {
                point = rnd.Next(4);
                Move(point);
            }
        }

        private void Move(int point) //0 - лево, 1 - верх, 2 - право, 3 - низ
        {
            int[] place;
            int i;
            int j;

            place = FindZero();
            i = place[0];
            j = place[1];
            switch (point)
            {
                case 0:
                    if (i > 0)
                    {
                        field[i, j] = field[i - 1, j];
                        field[i - 1, j] = 0;
                        actions++;
                    }
                    break;
                case 1:
                    if (j > 0)
                    {
                        field[i, j] = field[i, j - 1];
                        field[i, j - 1] = 0;
                        actions++;
                    }
                    break;
                case 2:
                    if (i < width_val - 1)
                    {
                        field[i, j] = field[i + 1, j];
                        field[i + 1, j] = 0;
                        actions++;
                    }
                    break;
                case 3:
                    if (j < height_val - 1)
                    {
                        field[i, j] = field[i, j + 1];
                        field[i, j + 1] = 0;
                        actions++;
                    }
                    break;
                case 4: //сдвиг влево
                    while (j < height_val - 1)
                    {
                        field[i, j] = field[i, j + 1];
                        field[i, j + 1] = 0;
                        j++;
                        actions++;
                    }
                    break;
                case 5: //сдвиг вверх
                    while (i < width_val - 1)
                    {
                        field[i, j] = field[i + 1, j];
                        field[i + 1, j] = 0;
                        i++;
                        actions++;
                    }
                    break;
                case 6: //сдвиг вниз
                    while (i > 0)
                    {
                        field[i, j] = field[i - 1, j];
                        field[i - 1, j] = 0;
                        i--;
                        actions++;
                    }
                    break;
                case 7: //сдвиг вправо
                    while (j > 0)
                    {
                        field[i, j] = field[i, j - 1];
                        field[i, j - 1] = 0;
                        j--;
                        actions++;
                    }
                    break;
            }
        }
        private int[] FindZero()
        {
            int[] point;

            point = new int[2];
            for (int i = 0; i < width_val; i++)
            {
                for (int j = 0; j < height_val; j++)
                {
                    if (field[i, j] == 0)
                    {
                        point[0] = i;
                        point[1] = j;
                    }
                }
            }
            return point;
        }
        private void PrintField()
        {
            for (int x = 0; x < width_val; x++)
                Console.Write("========");
            Console.Write("\n");
            for (int i = 0; i < height_val; i++)
            {
                for (int j = 0; j < width_val; j++)
                {
                    if (field[i, j] == 0)
                        Console.Write("| \t|");
                    else
                        Console.Write("|  {0}\t|", field[i, j]);
                }
                Console.Write("\n");
                for (int y = 0; y < width_val; y++)
                    Console.Write("========");
                Console.Write("\n");
            }
        }
        private bool Validate()
        {
            int value;

            value = 0;
            for (int i = 0; i < width_val; i++)
            {
                for (int j = 0; j < height_val; j++)
                {
                    if (field[i, j] != value)
                        return (false);
                    value++;
                }
            }
            return (true);
        } 
 */
