﻿using ClassLibrary1;
using System;

namespace Pyatnashki
{
    class Program
    {
        static void Main(string[] args)
        {
            int color;
            int width;
            int height;
            int shafls;

            Class1 game;

            color = 2;
            width = 4;
            height = 4;
            shafls = 20;

            Console.WriteLine("Выберите цвет консоли (F1 - синий, F2 - зеленый, F3 - белый)");
            if (Console.ReadKey(true).Key == ConsoleKey.F1)
                color = 0;
            if (Console.ReadKey(true).Key == ConsoleKey.F2)
                color = 1;
            if (Console.ReadKey(true).Key == ConsoleKey.F3)
                color = 2;
            Console.WriteLine("Выберите уровень сложности (F1 - первый, F2 - второй, F3 - третий)");
            if (Console.ReadKey(true).Key == ConsoleKey.F1)
            {
                width = 3;
                height = 3;
                shafls = 10;
            }
            if (Console.ReadKey(true).Key == ConsoleKey.F2)
            {
                width = 4;
                height = 4;
                shafls = 20;
            }
            if (Console.ReadKey(true).Key == ConsoleKey.F3)
            {
                width = 5;
                height = 5;
                shafls = 30;
            }
            game = new Class1(width, height, shafls, color);
            game.Gamestart();
            Console.Clear();
            Console.WriteLine("Поздравляю! Вы победили!");
        }
    }
}
