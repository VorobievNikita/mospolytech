﻿using System;

namespace ClassLibrary1
{
    public class Class1
    {
        int width_val;
        int height_val;
        int shafls;
        bool end;
        int actions;
        int[,] field;

        Random rnd;
        public Class1(int width, int height, int shafls, int color)
        {
            width_val = width;
            height_val = height;
            this.shafls = shafls;
            field = new int[width_val, height_val];
            end = false;
            switch (color)
            {
                case 0:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
            }
        }

        public void Gamestart()
        {
            int point;

            point = 0;

            DateTime dt = DateTime.Now;
            TimeSpan tmp;

            CreateField(field, width_val, height_val);
            Shafl();
            actions = 0;
            PrintField();
            while (!end)
            {
                if (Console.ReadKey(true).Key == ConsoleKey.UpArrow)
                    point = 2;
                if (Console.ReadKey(true).Key == ConsoleKey.DownArrow)
                    point = 0;
                if (Console.ReadKey(true).Key == ConsoleKey.LeftArrow)
                    point = 3;
                if (Console.ReadKey(true).Key == ConsoleKey.RightArrow)
                    point = 1;
                if (Console.ReadKey(true).Key == ConsoleKey.A)
                    point = 4;
                if (Console.ReadKey(true).Key == ConsoleKey.W)
                    point = 5;
                if (Console.ReadKey(true).Key == ConsoleKey.S)
                    point = 6;
                if (Console.ReadKey(true).Key == ConsoleKey.D)
                    point = 7;
                Move(field, point);
                end = Validate(field, width_val, height_val);
                Console.Clear();
                Console.WriteLine("=====================================================================");
                Console.Write("                Ходов: {0}| Время игры: ", actions);
                tmp = DateTime.Now - dt;
                Console.Write(tmp.Minutes);
                Console.Write(":");
                Console.WriteLine(tmp.Seconds);
                Console.WriteLine("=====================================================================");
                PrintField();
            }
        }
        public void CreateField(int[,] field, int width_val, int height_val)
        {
            int value;

            value = 0;
            for (int i = 0; i < width_val; i++)
            {
                for (int j = 0; j < height_val; j++)
                {
                    field[i, j] = value;
                    value++;
                }
            }
        }
        public void Shafl()
        {
            int point;
            rnd = new Random();

            for (int i = 0; i < shafls; i++)
            {
                point = rnd.Next(4);
                Move(field, point);
            }
        }

        public void Move(int[,] field, int point) //0 - лево, 1 - верх, 2 - право, 3 - низ
        {
            int[] place;
            int i;
            int j;

            place = FindZero(field);
            i = place[0];
            j = place[1];
            switch (point)
            {
                case 0:
                    if (i > 0)
                    {
                        field[i, j] = field[i - 1, j];
                        field[i - 1, j] = 0;
                        actions++;
                    }
                    break;
                case 1:
                    if (j > 0)
                    {
                        field[i, j] = field[i, j - 1];
                        field[i, j - 1] = 0;
                        actions++;
                    }
                    break;
                case 2:
                    if (i < width_val - 1)
                    {
                        field[i, j] = field[i + 1, j];
                        field[i + 1, j] = 0;
                        actions++;
                    }
                    break;
                case 3:
                    if (j < height_val - 1)
                    {
                        field[i, j] = field[i, j + 1];
                        field[i, j + 1] = 0;
                        actions++;
                    }
                    break;
                case 4: //сдвиг влево
                    while (j < height_val - 1)
                    {
                        field[i, j] = field[i, j + 1];
                        field[i, j + 1] = 0;
                        j++;
                        actions++;
                    }
                    break;
                case 5: //сдвиг вверх
                    while (i < width_val - 1)
                    {
                        field[i, j] = field[i + 1, j];
                        field[i + 1, j] = 0;
                        i++;
                        actions++;
                    }
                    break;
                case 6: //сдвиг вниз
                    while (i > 0)
                    {
                        field[i, j] = field[i - 1, j];
                        field[i - 1, j] = 0;
                        i--;
                        actions++;
                    }
                    break;
                case 7: //сдвиг вправо
                    while (j > 0)
                    {
                        field[i, j] = field[i, j - 1];
                        field[i, j - 1] = 0;
                        j--;
                        actions++;
                    }
                    break;
            }
        }
        public int[] FindZero(int[,] field)
        {
            int[] point;

            point = new int[2];
            for (int i = 0; i < width_val; i++)
            {
                for (int j = 0; j < height_val; j++)
                {
                    if (field[i, j] == 0)
                    {
                        point[0] = i;
                        point[1] = j;
                    }
                }
            }
            return point;
        }
        public void PrintField()
        {
            for (int x = 0; x < width_val; x++)
                Console.Write("========");
            Console.Write("\n");
            for (int i = 0; i < height_val; i++)
            {
                for (int j = 0; j < width_val; j++)
                {
                    if (field[i, j] == 0)
                        Console.Write("| \t|");
                    else
                        Console.Write("|  {0}\t|", field[i, j]);
                }
                Console.Write("\n");
                for (int y = 0; y < width_val; y++)
                    Console.Write("========");
                Console.Write("\n");
            }
        }
        public bool Validate(int[,] field, int width_val, int height_val)
        {
            int value;

            value = 0;
            for (int i = 0; i < width_val; i++)
            {
                for (int j = 0; j < height_val; j++)
                {
                    if (field[i, j] != value)
                        return (false);
                    value++;
                }
            }
            return (true);
        }
    }
}
