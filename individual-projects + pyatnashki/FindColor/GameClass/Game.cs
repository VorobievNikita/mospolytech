﻿using System;

namespace GameClass
{
    public class Game
    {
        int lvl;
        int[,] field;
        DateTime dt;

        public Game(int lvl)
        {
            this.lvl = lvl;
            dt = DateTime.Now;

            field = new int[lvl + 4, lvl + 4];
            Console.Clear();
        }

        public void playGame()
        {
            Random random;
            TimeSpan tmpTime;
            int corrects;
            int answer;
            int toFind;
            int tmp;
            int right;

            right = 0;
            for (int i = 0; i < 10; i++)
            {
                tmp = 10;
                random = new Random();
                field = new int[lvl + 4, lvl + 4];
                answer = 0;
                corrects = 0;
                toFind = random.Next(lvl);
                tmpTime = DateTime.Now - dt;

                createField(field, lvl);

                corrects = correctAnswer(toFind, field);
                Console.WriteLine("===================================================");
                Console.Write("Правильных ответов: {0}/10, время в игре: ", right);

                Console.Write(tmpTime.Minutes);
                Console.Write(":");
                Console.WriteLine(tmpTime.Seconds);
                Console.WriteLine("===================================================");
                printFiled();
                Console.WriteLine("");
                Console.Write("Введите количество ");
                switch (toFind)
                {
                    case 0:
                        Console.Write("красных фигур\n");
                        break;
                    case 1:
                        Console.Write("зеленых фигур\n");
                        break;
                    case 2:
                        Console.Write("синих фигур\n");
                        break;
                    case 3:
                        Console.Write("жёлтых фигур\n");
                        break;
                }
                while (tmp > 0)
                {
                    System.Threading.Thread.Sleep(1000);
                    Console.WriteLine("{0}", tmp);
                    tmp--;
                }
                Console.Clear();
                answer = int.Parse(Console.ReadLine());
                if (answer == corrects)
                {
                    Console.WriteLine("Правильно)");
                    right++;
                }
                else
                    Console.WriteLine("Ошибка(");
                Console.Clear();
            }
            Console.Clear();
            Console.WriteLine("Итоговый счет: {0} правильных ответов из 10, спасибо за игру!", right);
            System.Threading.Thread.Sleep(20000);
        }

        public void createField(int[,] field, int lvl)
        {
            Random random;
            
            random = new Random();

            for(int i = 0; i < 3 + lvl; i++)
            {
                for(int j = 0; j < 3 + lvl; j++)
                    field[i, j] = random.Next(lvl + 1);
            }
        }

        private void printFiled()
        {
            Random random;
            int symb;
            string pic;

            random = new Random();

            printHead(lvl);
            for (int i = 0; i < 3 + lvl; i++)
            {
                for (int j = 0; j < 3 + lvl; j++)
                {
                    symb = random.Next(3, 5);
                    pic = "";
                    switch (symb)
                    {
                        case 3:
                            pic = "/\\";
                            break;
                        case 4:
                            pic = "[]";
                            break;
                        case 5:
                            pic = "()";
                            break;
                    }
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("| ");
                    colorConsol(field[i, j]);
                    Console.Write(pic);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(" |");
                }
                Console.ForegroundColor = ConsoleColor.White;
                printHead(lvl);
            }
        }
        private void colorConsol(int color)
        {
            switch (color)
            {
                case 0:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }
        }
        private void printHead(int lvl)
        {
            Console.Write("\n");
            while(lvl + 3 > 0)
            {
                Console.Write("======");
                lvl--;
            }
            Console.Write("\n");
        }
        public int correctAnswer(int pic, int[,] feald)
        {
            int result;

            result = 0;
            for (int i = 0; i < 3 + lvl; i++)
            {
                for (int j = 0; j < 3 + lvl; j++)
                    if (feald[i, j] == pic)
                        result++;
            }
            return result;
        }
    }
}
