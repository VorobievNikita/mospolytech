using GameClass;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        Game game;
        int[,] field;

        [TestMethod]
        public void TestCreate()
        {
            game = new Game(1);
            field = new int[5, 5];

            game.createField(field, 1);
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                    if (field[i, j] < 0 || field[i, j] > 2)
                        Assert.AreEqual(0, 1);
            }
            Assert.AreEqual(0, 0);
        }
        [TestMethod]
        public void TestCorrect()
        {
            game = new Game(1);
            field = new int[5, 5];

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                    field[i, j] = 1;
            }
            Assert.AreEqual(game.correctAnswer(1, field), 25);
        }
    }
}