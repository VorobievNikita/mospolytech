﻿using GameClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindColor
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game;

            Console.WriteLine("Выберите уровень сложности (F1 - простой F2 - средний F3 - сложный)");
            ConsoleKeyInfo key = Console.ReadKey(true);
            if (key.Key == ConsoleKey.F1)
            {
                game = new Game(1);
                game.playGame();
            }
            else if (key.Key == ConsoleKey.F2)
            {
                game = new Game(2);
                game.playGame();
            }
            else if (key.Key == ConsoleKey.F3)
            {
                game = new Game(3);
                game.playGame();
            }
        }
    }
}
