﻿using System;
namespace ustnij_schet
{
    public class InterfaceClass
    {
        CreateExampleClass ce;
        int corrects;
        int errors;
        int marker = 1;
        double result = 0.0;
        bool cor = false;

        public InterfaceClass(int lvl)
        {
            ce = new CreateExampleClass(lvl);
            while (marker == 1)
                Play();
        }

        private void Play()
        {
            string ans;
            int iAns = 0;
            int m = 0;

            while (m != 1)
            {
                Console.WriteLine("Для просмотра счета введите 1, для игры введите 2, для выхода введите 3");
                ans = Console.ReadLine();
                if (ans != "1" || ans != "2" || ans != "3")
                {
                    iAns = int.Parse(ans);
                    m = 1;
                }
                else
                    Console.WriteLine("Ошибка");
            }
            switch (iAns)
            {
                case 1:
                    marker = 1;
                    Console.Clear(); 
                    Console.WriteLine("---------------------------------------Счет---------------------------------------");
                    Console.WriteLine("Правильных ответов: {0}, ошибок: {1}", corrects, errors);
                    Console.WriteLine("----------------------------------------------------------------------------------");
                    break;
                case 2:
                    rounds();
                    break;
                default:
                    marker = 0;
                    break;
            }
        }
        private void rounds()
        {
            for (int k = 0; k < 10; k++)
            {
                marker = 1;
                Console.Clear();
                ce.createEx();
                for (int j = 0; j < 10; j++)
                {
                    Console.Write("-------------------------------------Раунд: {0}------------------------------------", k);
                    if (k != 10)
                        Console.Write("-\n");
                    else
                        Console.Write("\n");
                    Console.WriteLine("Прогресс:");
                    for (int x = 0; x < k; x++)
                    {
                        for (int o = 0; o < 9; o++)
                            Console.Write("#");
                    }
                    for (int y = k + 1; y < 10; y++)
                    {
                        for (int o = 0; o < 9; o++)
                            Console.Write("=");
                    }
                    if(k != 10)
                        Console.Write("=\n");
                    else
                        Console.Write("#\n");
                    Console.WriteLine(ce.getResultEx());
                    Console.Write("-------------------------------------Время: {0}------------------------------------", 10 - j);
                    if (j != 10)
                        Console.Write("-\n");
                    else
                        Console.Write("\n");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                }
                Console.Write("-------------------------------------Раунд: {0}------------------------------------", k);
                if (k != 10)
                    Console.Write("-\n");
                while (!cor)
                {
                    Console.WriteLine("Введите результат");
                    cor = double.TryParse(Console.ReadLine(), out result);
                }
                cor = false;
                if (ce.getAnswer(result))
                {
                    Console.Clear();
                    Console.Write("-------------------------------------Раунд: {0}------------------------------------", k);
                    if (k != 10)
                        Console.Write("-\n");
                Console.WriteLine("                                     Правильно!                                     ");
                    corrects++;
                    System.Threading.Thread.Sleep(2000);
                }
                else
                {
                    Console.WriteLine("Ошибка");
                    errors++;
                    System.Threading.Thread.Sleep(2000);
                }
            }
        }
    }
}
