﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ustnij_schet
{
    public class CalculateExpression
    {
        private static readonly Dictionary<string, byte> _opdict = new Dictionary<string, byte>
            {{"(", 0}, {"+", 1}, {"-", 2},{"/", 3}, {"*", 4}};

        private static readonly HashSet<string> _operators = new HashSet<string> { "+", "-", "/", "*" };

        private static string _input;

        private static double CalculateSimple(double a, double b, char op)
        {
            switch (op)
            {
                case '+':
                    return a + b;
                case '-':
                    return a - b;
                case '*':
                    return a * b;
                case '/':
                    return a / b;
            }
            return 0;
        }

        private static double StackCalc(double a, double b, char op)
        {
            return CalculateSimple(b, a, op);
        }

        private static bool BadBreckets(string s)
        {
            int i = 0;
            foreach (char c in s)
            {
                if (c == '(')
                    i++;
                else if (c == ')')
                    i--;
            }
            return i != 0;
        }

        private static string Reverse(string s)
        {
            int length = s.Length;
            var sb = new StringBuilder(length, length);
            for (int i = length - 1; i >= 0; i--)
                sb.Append(s[i]);
            return sb.ToString();
        }

        public static string ToRPN()
        {
            var result = new StringBuilder(_input.Length);
            bool negative = false;
            var stack = new Stack<char>();
            foreach (char c in _input)
            {
                if (c == ' ') continue;
                if (c == '(')
                    stack.Push(c);
                else if (c == ')')
                {
                    while (stack.Count > 0)
                    {
                        char a = stack.Pop();
                        if (a == '(') break;
                        result.Append(" " + a);
                    }
                }
                else if (_operators.Contains(c.ToString()))
                {
                    string op = c.ToString();
                    if (c == '-')
                    {
                        negative = true;
                        result.Append("0 ");
                        op = "+";
                    }
                    else
                        negative = false;
                    result.Append(" ");
                    while (stack.Count > 0 && _opdict[op] < _opdict[stack.Peek().ToString()])
                    {
                        result.Append(stack.Pop() + " ");
                    }
                    stack.Push(op[0]);
                }
                else
                {
                    if (negative)
                    {
                        result.Append('-');
                        negative = false;
                    }
                    result.Append(c);
                }
            }
            while (stack.Count > 0)
                result.Append(" " + stack.Pop());
            return result.ToString();
        }

        public static string ToPN()
        {
            return Reverse(ToRPN());
        }

        public static double Calculate(string input)
        {
            if (BadBreckets(input) || String.IsNullOrEmpty(input))
                return double.NaN;
            _input = input;
            string[] strings = ToRPN().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var stack = new Stack<double>(input.Length / 2);
            foreach (string s in strings)
            {
                if (_opdict.ContainsKey(s))
                {
                    if (stack.Count < 2)
                        if (s == "+" || s == "-")
                        {
                            stack.Push(StackCalc(stack.Pop(), 0, s[0]));
                            continue;
                        }
                        else return double.NaN;
                    stack.Push(StackCalc(stack.Pop(), stack.Pop(), s[0]));
                }
                else
                {
                    double a;
                    if (!double.TryParse(s, out a)) return double.NaN;
                    stack.Push(a);
                }
            }
            return Math.Round(stack.Peek(), 15);
        }
    }
}
