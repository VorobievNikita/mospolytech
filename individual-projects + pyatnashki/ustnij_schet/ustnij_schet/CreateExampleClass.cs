﻿using System;
namespace ustnij_schet
{
    public class CreateExampleClass
    {
        int lvl;
        string resultEx;
        double answer;
        Random random = new Random();

        public CreateExampleClass(int lvl)
        {
            this.lvl = lvl;
        }

        public string getResultEx()
        {
            return (resultEx);
        }

        public bool getAnswer(double answer)
        {
            if (answer - this.answer != 0)
                return (false);
            return (true);
        }

        public void createEx()
        {
            resultEx = null;
            int elems;

            elems = random.Next(2, 5);
            while (elems > 0)
            {
                resultEx += random.Next(1, 100).ToString();
                if (elems != 1)
                    resultEx += action();
                elems--;
            }
            answer = CalculateExpression.Calculate(resultEx);
        }

        private char action()
        {
            int tmp = 0;
            switch(lvl)
            {
                case 1:
                    tmp = random.Next(1, 2);
                    break;
                case 2:
                    tmp = random.Next(1, 3);
                    break;
                case 3:
                    tmp = random.Next(1, 4);
                    break;
            }
            switch (tmp)
            {
                case 1:
                    return ('+');
                case 2:
                    return ('-');
                case 3:
                    return ('*');
                case 4:
                    return ('/');
            }
            return (' ');
        }
    }
}
