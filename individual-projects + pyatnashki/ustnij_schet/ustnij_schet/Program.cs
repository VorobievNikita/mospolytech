﻿using System;

namespace ustnij_schet
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int lvl = 1;
            string slvl;
            int marker = 0;
            InterfaceClass ic;

            while (marker != 1)
            {
                Console.WriteLine("Выберите уровень сложности 1-3");
                slvl = Console.ReadLine();
                if (slvl != "1" || slvl != "2" || slvl != "3")
                {
                    lvl = int.Parse(slvl);
                    marker = 1;
                }
                if (marker == 0)
                    Console.WriteLine("Ошибка!");
            }
            ic = new InterfaceClass(lvl);
        }
    }
}
